# FeedAd iOS SDK Demo Apps

This repository contains two demo apps that show how to configure and integrate the FeedAd SDK into an iOS app written in Objective-C and Swift.


## FeedAdExampleObjc: Getting Started

1. `cd FeedAdExampleObjc/`
2. `pod install`
3. `open FeedAdExampleObjc.xcworkspace`
4. In the file *FAGlobals.h*: Update the line `#define FA_CLIENT_TOKEN @"<your-client-token>"` with your FeedAd client token


## FeedAdExampleSwift: Getting Started

1. `cd FeedAdExampleSwift/`
2. `pod install`
3. `open FeedAdExampleSwift.xcworkspace`
4. In the file *FAGlobals.swift*: Update the line `static let ClientToken = "<your-client-token>"` with your FeedAd client token

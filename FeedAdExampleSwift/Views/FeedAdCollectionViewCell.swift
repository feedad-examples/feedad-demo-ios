//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

import UIKit

class FeedAdCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        for subview in containerView.subviews {
            subview.removeFromSuperview()
        }
    }

    func injectAdView(_ adView: UIView) {
        adView.frame = containerView.bounds
        adView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        containerView.addSubview(adView)
    }
    
}

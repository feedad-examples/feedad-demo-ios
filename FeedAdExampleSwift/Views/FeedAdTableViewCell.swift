//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

import UIKit

class FeedAdTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        for subview in containerView.subviews {
            subview.removeFromSuperview()
        }
    }
    
    func injectAdView(_ adView: UIView) {
        if let adView = adView as? GAMBannerView {
            // We must use [GAMBannerView resize:] whenever its
            // size has to change - otherwise it will simply
            // cancel the display of the ad
            adView.resize(GADAdSizeFromCGSize(containerView.bounds.size))
        } else {
            adView.frame = containerView.bounds
            adView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        }
        
        containerView.addSubview(adView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard subviews.count > 0 else {
            return
        }
        
        if let adView = subviews[0] as? GAMBannerView {
            // We must use [GAMBannerView resize:] whenever its
            // size has to change - otherwise it will simply
            // cancel the display of the ad
            adView.resize(GADAdSizeFromCGSize(containerView.bounds.size))
        }
    }
    
}

//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

import UIKit

class NewsEntryParagraphTableViewCell: UITableViewCell {
    
    @IBOutlet weak var paragraphLabel: UILabel!
    
    static let NewsEntryParagraphTableViewCellHorizontalMargin: CGFloat = 8.0 * 2.0
    static let NewsEntryParagraphTableViewCellVerticalMargin: CGFloat = 8.0 * 2.0
    
    static func heightForParagraph(_ paragraph: String, withWidth width: CGFloat) -> CGFloat {
        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17.0)]
        let boundingSize = CGSize(width: width - NewsEntryParagraphTableViewCellVerticalMargin, height: CGFloat.greatestFiniteMagnitude)
        let boundingRect = NSString(string: paragraph).boundingRect(with: boundingSize, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
        return ceil(boundingRect.size.height)
    }
    
    func setup(from paragraph: String) {
        paragraphLabel.text = paragraph
    }
    
}

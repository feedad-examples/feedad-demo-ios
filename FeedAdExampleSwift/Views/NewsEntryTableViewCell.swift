//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

import UIKit

class NewsEntryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    func setup(from entry: NewsEntry) {
        avatarImageView.image = UIImage.init(named: entry.avatarImageName)
        bodyLabel.text        = entry.body
        subtitleLabel.text    = entry.subtitle
        titleLabel.text       = entry.title
    }
}

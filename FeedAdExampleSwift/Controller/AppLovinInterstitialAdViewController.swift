//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

import UIKit
import FeedAd


enum AppLovinInterstitialAdViewControllerState {
    case initializing
    case initialized
    case loading
    case loaded
    case showing
}


class AppLovinInterstitialAdViewController: UIViewController {
    
    // See FAGlobals.swift for the constant AppLovinKey
    
    // Replace this with the ID of your AppLovin ad unit
    //
    // An ad unit must be added in the AppLovin dashboard
    // for the bundle ID of your app, before using this example.
    // For setup instructions see the AppLovin mediation docs
    // at docs.feedad.com.
    //
    // Please note it might take half an hour or longer for
    // any updates in the AppLovin dashboard to take effect
    // due to caching.
    //
    // Also, currently, custom SDK mediation is known to
    // only work on real devices -- you may only receive
    // test ads from AppLovin on iOS simulator.
    let APPLOVIN_ADUNIT_ID = ""
    
    var appLovinInterstitialAd: MAInterstitialAd? = nil
    var state: AppLovinInterstitialAdViewControllerState = .initializing {
        didSet {
            var shouldEnableLoadButton = false
            var shouldEnableShowButton = false
            
            if (state == .initialized) {
                shouldEnableLoadButton = true
            } else if (state == .loaded) {
                shouldEnableShowButton = true
            }
            
            // activityIndicatorView
            self.activityIndicatorView.isHidden = state != .initializing && state != .loading && state != .showing
            
            // loadButton
            self.loadButton.isUserInteractionEnabled = shouldEnableLoadButton;
            self.loadButton.alpha                    = shouldEnableLoadButton ? 1.0 : 0.5
            
            // showButton
            self.showButton.isUserInteractionEnabled = shouldEnableShowButton;
            self.showButton.alpha                    = shouldEnableShowButton ? 1.0 : 0.5
        }
    }
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var loadButton: UIButton!
    @IBOutlet weak var showButton: UIButton!
    
    
    // MARK: - View Lifecycle
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        startShowingAds()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        stopShowingAds()
    }
    
    
    // MARK: - Actions
    
    @IBAction func didTapLoadButton(_ sender: Any) {
        self.state = .loading
        
        // Load interstitial ad
        self.appLovinInterstitialAd?.load()
    }
    
    @IBAction func didTapShowButton(_ sender: Any) {
        self.state = .showing
        
        // Show loaded interstitial ad
        self.appLovinInterstitialAd?.show()
    }
}

// MARK: - Interstitial Ad

extension AppLovinInterstitialAdViewController {
    
    func startShowingAds() {
        if let _ = appLovinInterstitialAd {
            return
        }
        
        self.state = .initializing
        
        // Initialize the AppLovin SDK
        //
        // Normally this would be done in the app delegate
        let appLovinConfig = ALSdkInitializationConfiguration(sdkKey: FAGlobals.AppLovinKey) { builder in
            builder.mediationProvider = ALMediationProviderMAX
        }
        
        let appLovinSdk = ALSdk.shared()
        appLovinSdk.initialize(with: appLovinConfig) { [weak self] _ in
            guard let self = self else {
                return
            }
            
            // Setup MAInterstitialAd
            let appLovinInterstitialAd = MAInterstitialAd(adUnitIdentifier: self.APPLOVIN_ADUNIT_ID, sdk: appLovinSdk)
            appLovinInterstitialAd.delegate = self
            self.appLovinInterstitialAd = appLovinInterstitialAd
            
            self.state = .initialized
        }
    }
    
    func stopShowingAds() {
        appLovinInterstitialAd?.delegate = nil
        appLovinInterstitialAd = nil
    }
    
}

// MARK: - MAAdDelegate

extension AppLovinInterstitialAdViewController: MAAdDelegate {
     
    func didClick(_ ad: MAAd) {
        print("***** \(#function): ad = \(ad)")
    }
    
    func didDisplay(_ ad: MAAd) {
        print("***** \(#function): ad = \(ad)")
    }
    
    func didFail(toDisplay ad: MAAd, withError error: MAError) {
        print("***** \(#function): ad = \(ad), error = \(error)")
        
        self.state = .initialized
    }
    
    func didFailToLoadAd(forAdUnitIdentifier adUnitIdentifier: String, withError error: MAError) {
        print("***** \(#function): adUnitIdentifier = \(adUnitIdentifier), error = \(error)")
        
        self.state = .initialized
    }
    
    func didHide(_ ad: MAAd) {
        print("***** \(#function): ad = \(ad)")
        
        self.state = .initialized
    }
    
    func didLoad(_ ad: MAAd) {
        print("***** \(#function): ad = \(ad)")
        
        self.state = .loaded
    }
    
}

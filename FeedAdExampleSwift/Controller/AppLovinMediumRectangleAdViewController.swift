//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

import UIKit
import FeedAd
import AppLovinSDK

class AppLovinMediumRectangleAdViewController: UIViewController {
    
    // See FAGlobals.swift for the constant AppLovinKey
    
    // Replace this with the ID of your AppLovin ad unit
    //
    // An ad unit must be added in the AppLovin dashboard
    // for the bundle ID of your app, before using this example.
    // For setup instructions see the AppLovin mediation docs
    // at docs.feedad.com.
    //
    // Please note it might take half an hour or longer for
    // any updates in the AppLovin dashboard to take effect
    // due to caching.
    //
    // Also, currently, custom SDK mediation is known to
    // only work on real devices -- you may only receive
    // test ads from AppLovin on iOS simulator.
    let APPLOVIN_ADUNIT_ID = ""
    
    @IBOutlet weak var tableView: UITableView!
    
    var appLovinMediumRectangleAd: MAAdView?
    var displayData = [Any]()
    var data = [NewsEntry]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        updateTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startShowingAds()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopShowingAds()
    }
    
}


// MARK: - Ads

extension AppLovinMediumRectangleAdViewController {
    
    func startShowingAds() {
        guard self.appLovinMediumRectangleAd == nil else {
            return
        }
        
        // Initialize the AppLovin SDK
        //
        // Normally this would be done in the app delegate
        
        let appLovinConfig = ALSdkInitializationConfiguration(sdkKey: FAGlobals.AppLovinKey) { builder in
            builder.mediationProvider = ALMediationProviderMAX
        }
        
        let appLovinSdk = ALSdk.shared()
        appLovinSdk.initialize(with: appLovinConfig) { [weak self] _ in
            guard let self = self else {
                return
            }
            
            // Setup MAAdView and load a medium rectangle ad
            let appLovinMediumRectangleAd = MAAdView(adUnitIdentifier: self.APPLOVIN_ADUNIT_ID, adFormat: MAAdFormat.mrec, sdk: appLovinSdk)
            appLovinMediumRectangleAd.delegate = self
            appLovinMediumRectangleAd.frame = CGRect(x: 0.0, y: 0.0, width: 300.0, height: 250.0)
            self.appLovinMediumRectangleAd = appLovinMediumRectangleAd
            
            appLovinMediumRectangleAd.loadAd()
        }
    }
    
    func stopShowingAds() {
        guard let appLovinMediumRectangleAd = self.appLovinMediumRectangleAd else {
            return
        }
        
        appLovinMediumRectangleAd.delegate = self
        appLovinMediumRectangleAd.removeFromSuperview()
        self.appLovinMediumRectangleAd = nil
        
        self.updateTableView()
    }
    
}


// MARK: - Setup Data

extension AppLovinMediumRectangleAdViewController {
    
    func injectAds(_ data: [NewsEntry]) -> [Any] {
        guard let MediumRectangleAd = self.appLovinMediumRectangleAd else {
            return data
        }
        
        let start = 2
        let gapItemCount = 7
        
        if data.count < start {
            return data
        }
        
        var injectedData = [Any]()
        injectedData.append(contentsOf: data as [Any])
        
        var i = start
        while i < injectedData.count {
            injectedData.insert(MediumRectangleAd, at: i)
            i += gapItemCount
        }
        
        return injectedData
    }
    
    func loadData() {
        let newsEntry = NewsEntry()
        
        var data = [NewsEntry]()
        
        for _ in stride(from: 0, to: 15, by: 1) {
            data.append(newsEntry)
        }
        
        self.data = data
    }
    
    func updateTableView() {
        displayData = injectAds(data)
        tableView.reloadData()
    }
    
}


// MARK: - MAAdViewAdDelegate

extension AppLovinMediumRectangleAdViewController: MAAdViewAdDelegate {
    
    func didClick(_ ad: MAAd) {
        print("***** \(#function): ad = \(ad)")
    }
    
    func didCollapse(_ ad: MAAd) {
        print("***** \(#function): ad = \(ad)")
    }
    
    func didDisplay(_ ad: MAAd) {
        print("***** \(#function): ad = \(ad)")
    }
    
    func didExpand(_ ad: MAAd) {
        print("***** \(#function): ad = \(ad)")
    }
    
    func didFail(toDisplay ad: MAAd, withError error: MAError) {
        print("***** \(#function): ad = \(ad), error = \(error)")
    }
    
    func didFailToLoadAd(forAdUnitIdentifier adUnitIdentifier: String, withError error: MAError) {
        print("***** \(#function): adUnitIdentifier = \(adUnitIdentifier), error = \(error)")
    }
    
    func didHide(_ ad: MAAd) {
        print("***** \(#function): ad = \(ad)")
    }
    
    func didLoad(_ ad: MAAd) {
        print("***** \(#function): ad = \(ad), ad.networkName = \(ad.networkName)")
        
        self.updateTableView()
    }
    
}

// MARK: - UITableViewDelegate

extension AppLovinMediumRectangleAdViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedElement = displayData[indexPath.row] as? NewsEntry {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String(describing: NewsEntryViewController.self)) as! NewsEntryViewController
            controller.entry = selectedElement
            controller.title = ""
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let currentElement = displayData[indexPath.row]
        
        if currentElement is MAAdView {
            return 250.0
        }
        
        return 169.0
    }
    
}

// MARK: - UITableViewDataSource

extension AppLovinMediumRectangleAdViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentElement = displayData[indexPath.row]
        
        if let adView = currentElement as? MAAdView {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FeedAdTableViewCell.self), for: indexPath) as! FeedAdTableViewCell
            cell.injectAdView(adView)
            return cell
        }
        
        let entry = currentElement as! NewsEntry
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewsEntryTableViewCell.self), for: indexPath) as! NewsEntryTableViewCell
        cell.setup(from: entry)
        return cell
    }
    
}

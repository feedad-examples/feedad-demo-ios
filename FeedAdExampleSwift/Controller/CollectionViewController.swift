//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

import UIKit
import FeedAd

class CollectionViewController : UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var feedAd: FAFeedAd? = nil
    var displayData = [Any]()
    var data = [NewsEntry]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        updateCollectionView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startShowingAds()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopShowingAds()
    }
}

// MARK: - Feed Ads

extension CollectionViewController {
    
    func startShowingAds() {
        if let _ = feedAd {
            return
        }
        
        let config = FAFeedAdConfig()
        config.placementId = "collection-view"
        
        feedAd = FAFeedAd(config: config)
        feedAd?.delegate = self
        feedAd?.load()
    }
    
    func stopShowingAds() {
        feedAd?.cancel()
        feedAd?.adView.removeFromSuperview()
        feedAd?.delegate = nil
        feedAd = nil 
        
        updateCollectionView()
    }
    
}

// MARK: - Setup Data

extension CollectionViewController {
    
    func injectAds(_ array: [NewsEntry]) -> [Any] {
        guard let feedAd = self.feedAd, feedAd.isLoaded else {
            return data
        }
        
        let start = 2
        let gapItemCount = 7
        
        if data.count < start {
            return data
        }
        
        var injectedData = [Any]()
        injectedData.append(contentsOf: data as [Any])
        
        var i = start
        while i < injectedData.count {
            injectedData.insert(feedAd, at: i)
            i += gapItemCount
        }
        
        return injectedData
    }
    
    func loadData() {
        let newsEntry = NewsEntry()
        for _ in stride(from: 0, to: 15, by: 1) {
            data.append(newsEntry)
        }
    }
    
    func updateCollectionView() {
        displayData = injectAds(data)
        collectionView.reloadData()
    }
}

// MARK: - FAFeedAdDelegate

extension CollectionViewController: FAFeedAdDelegate {
    
    func feedAd(_ feedAd: FAFeedAd, didFailWithError error: Error) {
        print("***** \(#function): feedAd = \(feedAd), error = \(error)")
        updateCollectionView()
    }
    
    func feedAdDidChangeSize(_ feedAd: FAFeedAd) {
        print("***** \(#function): feedAd = \(feedAd)")
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    func feedAdDidFinishLoading(_ feedAd: FAFeedAd) {
        print("***** \(#function): feedAd = \(feedAd)")
        updateCollectionView()
    }
    
    func feedAdDidFinishPlaying(_ feedAd: FAFeedAd) {
        print("***** \(#function): feedAd = \(feedAd)")
        updateCollectionView()
    }
    
}

// MARK: - UICollectionViewDelegate

extension CollectionViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let selectedElement = displayData[indexPath.item] as? NewsEntry {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String(describing: NewsEntryViewController.self)) as! NewsEntryViewController
            controller.entry = selectedElement
            controller.title = ""
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
}

// MARK: - UICollectionViewDataSource

extension CollectionViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let currentElement = displayData[indexPath.item]
        
        if let ad = currentElement as? FAFeedAd {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: FeedAdCollectionViewCell.self), for: indexPath) as! FeedAdCollectionViewCell
            cell.injectAdView(ad.adView)
            return cell
        }
        
        let entry = currentElement as! NewsEntry
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: NewsEntryCollectionViewCell.self), for: indexPath) as! NewsEntryCollectionViewCell
        cell.setup(from: entry)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return displayData.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
}

// MARK: UICollectionViewDelegateFlowLayout

extension CollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let currentElement = displayData[indexPath.item]
        if let ad = currentElement as? FAFeedAd {
            return ad.size(forSuperviewSize: CGSize(width: collectionView.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        }
        
        return CGSize(width: collectionView.frame.size.width, height: 169.0)
    }
    
}

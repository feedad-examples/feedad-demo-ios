//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

import UIKit
import FeedAd


enum AdMobInterstitialAdViewControllerState {
    case initializing
    case initialized
    case loading
    case loaded
    case showing
}


class AdMobInterstitialAdViewController: UIViewController {
    // Make sure to update GADApplicationIdentifier in your Info.plist
    let ADMOB_INTERSTITIALAD_ADUNIT_ID = "<your-admob-adunit-id>"
    
    var adMobInterstitialAd: GADInterstitialAd? = nil
    var state: AdMobInterstitialAdViewControllerState = .initializing {
        didSet {
            var shouldEnableLoadButton = false
            var shouldEnableShowButton = false
            
            if (state == .initialized) {
                shouldEnableLoadButton = true
            } else if (state == .loaded) {
                shouldEnableShowButton = true
            }
            
            // activityIndicatorView
            self.activityIndicatorView.isHidden = state != .initializing && state != .loading && state != .showing
            
            // loadButton
            self.loadButton.isUserInteractionEnabled = shouldEnableLoadButton;
            self.loadButton.alpha                    = shouldEnableLoadButton ? 1.0 : 0.5
            
            // showButton
            self.showButton.isUserInteractionEnabled = shouldEnableShowButton;
            self.showButton.alpha                    = shouldEnableShowButton ? 1.0 : 0.5
        }
    }
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var loadButton: UIButton!
    @IBOutlet weak var showButton: UIButton!
    
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.state = .initialized
    }
    
    
    // MARK: - Actions
    
    @IBAction func didTapLoadButton(_ sender: Any) {
        self.state = .loading
        
        // Load interstitial ad
        if self.adMobInterstitialAd != nil {
            return
        }
        
        self.state = .loading
        
        let request = GADRequest()
        GADInterstitialAd.load(withAdUnitID: ADMOB_INTERSTITIALAD_ADUNIT_ID, request: request) { [weak self] interstitialAd, error in
            print("***** \(#function): interstitialAd = \(String(describing: interstitialAd)), error = \(String(describing: error))")
            
            guard let self = self, error == nil else {
                return
            }
            
            interstitialAd?.fullScreenContentDelegate = self
            self.adMobInterstitialAd = interstitialAd
            self.state = .loaded
        }
    }
    
    @IBAction func didTapShowButton(_ sender: Any) {
        self.state = .showing
        
        // Show loaded interstitial ad
        self.adMobInterstitialAd?.present(fromRootViewController: self)
    }
}


// MARK: - GADFullScreenContentDelegate

extension AdMobInterstitialAdViewController: GADFullScreenContentDelegate {
    func ad(_ ad: any GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: any Error) {
        print("***** \(#function): ad = \(ad), error = \(error)")
        
        self.state = .initialized
    }
    
    func adDidDismissFullScreenContent(_ ad: any GADFullScreenPresentingAd) {
        print("***** \(#function): ad = \(ad)")
        
        self.adMobInterstitialAd = nil
        self.state = .initialized
    }
    
    func adWillPresentFullScreenContent(_ ad: any GADFullScreenPresentingAd) {
        print("***** \(#function): ad = \(ad)")
        
        self.state = .showing
    }
}

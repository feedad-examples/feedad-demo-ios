//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

import UIKit
import FeedAd
import AppLovinSDK

class AppLovinNativeAdViewController: UIViewController {
    
    // See FAGlobals.swift for the constant AppLovinKey
    
    // Replace this with the ID of your AppLovin ad unit
    //
    // An ad unit must be added in the AppLovin dashboard
    // for the bundle ID of your app, before using this example.
    // For setup instructions see the AppLovin mediation docs
    // at docs.feedad.com.
    //
    // Please note it might take half an hour or longer for
    // any updates in the AppLovin dashboard to take effect
    // due to caching.
    //
    // Also, currently, custom SDK mediation is known to
    // only work on real devices -- you may only receive
    // test ads from AppLovin on iOS simulator.
    let APPLOVIN_ADUNIT_ID = ""
    
    @IBOutlet weak var tableView: UITableView!
    
    var appLovinNativeAd: MAAd?
    var appLovinNativeAdView: MANativeAdView?
    var appLovinNativeAdLoader: MANativeAdLoader?
    var displayData = [Any]()
    var data = [NewsEntry]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        updateTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startShowingAds()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopShowingAds()
    }
    
}


// MARK: - Ads

extension AppLovinNativeAdViewController {
    
    func startShowingAds() {
        guard self.appLovinNativeAdLoader == nil else {
            return
        }
        
        // Initialize the AppLovin SDK
        //
        // Normally this would be done in the app delegate
        let appLovinConfig = ALSdkInitializationConfiguration(sdkKey: FAGlobals.AppLovinKey) { builder in
            builder.mediationProvider = ALMediationProviderMAX
        }
        
        let appLovinSdk = ALSdk.shared()
        appLovinSdk.initialize(with: appLovinConfig) { [weak self] _ in
            guard let self = self else {
                return
            }
            
            // Initialize MANativeAdView
            //
            // We use a subclass here that implements alternation of the layout for FeedAd
            // The subclass and XIB is part of the example code and can be customized as needed
            let appLovinnativeAdViewNib = UINib(nibName: "FAAppLovinManualNativeAdView", bundle: Bundle.main)
            self.appLovinNativeAdView = (appLovinnativeAdViewNib.instantiate(withOwner: nil, options: nil).first as! MANativeAdView)
            
            // Bind views using the tags specified previously in FAAppLovinManualNativeAdView.xib
            self.appLovinNativeAdView?.bindViews(with: MANativeAdViewBinder(builderBlock: { builder in
                builder.iconImageViewTag = 42
                builder.titleLabelTag = 43
                builder.advertiserLabelTag = 44
                builder.callToActionButtonTag = 45
                builder.mediaContentViewTag = 46
            }))
            
            // Load an ad
            //
            // When using the manual native ad approach, we have to use
            // loadAdIntoAdView: and have an instance of MANativeAdView
            // initialized and bound -- otherwise the SDK will only complain
            // with an error message or views will not be filled with content
            // from the native ad that was received
            let appLovinNativeAdLoader = MANativeAdLoader(adUnitIdentifier: self.APPLOVIN_ADUNIT_ID, sdk: appLovinSdk)
            appLovinNativeAdLoader.nativeAdDelegate = self
            appLovinNativeAdLoader.loadAd(into: self.appLovinNativeAdView)
            self.appLovinNativeAdLoader = appLovinNativeAdLoader
        }
    }
    
    func stopShowingAds() {
        guard let appLovinNativeAdLoader = self.appLovinNativeAdLoader else {
            return
        }
        
        if let appLovinNativeAd = self.appLovinNativeAd {
            appLovinNativeAdLoader.destroy(appLovinNativeAd)
            self.appLovinNativeAd = nil
        }
        
        if let appLovinNativeAdView = self.appLovinNativeAdView {
            appLovinNativeAdView.removeFromSuperview()
            self.appLovinNativeAdView = nil
        }
        
        appLovinNativeAdLoader.nativeAdDelegate = nil
        self.appLovinNativeAdLoader = nil
        
        self.updateTableView()
    }
    
}


// MARK: - Setup Data

extension AppLovinNativeAdViewController {
    
    func injectAds(_ data: [NewsEntry]) -> [Any] {
        guard let nativeAdView = self.appLovinNativeAdView else {
            return data
        }
        
        let start = 2
        let gapItemCount = 7
        
        if data.count < start {
            return data
        }
        
        var injectedData = [Any]()
        injectedData.append(contentsOf: data as [Any])
        
        var i = start
        while i < injectedData.count {
            injectedData.insert(nativeAdView, at: i)
            i += gapItemCount
        }
        
        return injectedData
    }
    
    func loadData() {
        let newsEntry = NewsEntry()
        
        var data = [NewsEntry]()
        
        for _ in stride(from: 0, to: 15, by: 1) {
            data.append(newsEntry)
        }
        
        self.data = data
    }
    
    func updateTableView() {
        displayData = injectAds(data)
        tableView.reloadData()
    }
    
}


// MARK: - MANativeAdDelegate

extension AppLovinNativeAdViewController: MANativeAdDelegate {
    
    func didClickNativeAd(_ ad: MAAd) {
        print("***** \(#function): ad = \(ad)")
    }
    
    func didFailToLoadNativeAd(forAdUnitIdentifier adUnitIdentifier: String, withError error: MAError) {
        print("***** \(#function): adUnitIdentifier = \(adUnitIdentifier), error = \(error)")
        
        // You may want to implement retries here
        self.stopShowingAds()
    }
    
    func didLoadNativeAd(_ nativeAdView: MANativeAdView?, for ad: MAAd) {
        print("***** \(#function): nativeAdView = \(String(describing: nativeAdView)), ad = \(ad)")
        
        // Clean up any pre-existing ad to prevent memory leaks
        if let appLovinNativeAd = self.appLovinNativeAd {
            self.appLovinNativeAdLoader?.destroy(appLovinNativeAd)
        }
        
        // Save ad for cleanup
        self.appLovinNativeAd = ad
        
        // Add Ad view to view
        if let appLovinNativeAdView = self.appLovinNativeAdView {
            appLovinNativeAdView.removeFromSuperview()
        }
        
        self.appLovinNativeAdView = nativeAdView
        
        // Update table view
        self.updateTableView()
    }
    
}

// MARK: - UITableViewDelegate

extension AppLovinNativeAdViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedElement = displayData[indexPath.row] as? NewsEntry {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String(describing: NewsEntryViewController.self)) as! NewsEntryViewController
            controller.entry = selectedElement
            controller.title = ""
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let currentElement = displayData[indexPath.row]
        
        if currentElement is FAAppLovinManualNativeAdView {
            return round(tableView.frame.size.width / 16.0 * 9.0) + 1.0
        }
        
        return 169.0
    }
    
}

// MARK: - UITableViewDataSource

extension AppLovinNativeAdViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentElement = displayData[indexPath.row]
        
        if let adView = currentElement as? FAAppLovinManualNativeAdView {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FeedAdTableViewCell.self), for: indexPath) as! FeedAdTableViewCell
            cell.injectAdView(adView)
            return cell
        }
        
        let entry = currentElement as! NewsEntry
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewsEntryTableViewCell.self), for: indexPath) as! NewsEntryTableViewCell
        cell.setup(from: entry)
        return cell
    }
    
}

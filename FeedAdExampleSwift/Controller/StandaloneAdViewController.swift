//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

import UIKit
import FeedAd

class StandaloneAdViewController: UIViewController {
    
    @IBOutlet weak var adContainerView: UIView!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var showButton: UIButton!
    @IBOutlet weak var testImageView: UIImageView!
    
    var standaloneAd: FAStandaloneAd? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Resize test image view
        var height = Float(testImageView.frame.size.width) / 16.0 * 9.0
        var originY = Float(view.frame.size.height) / 2.0 - height / 2.0
        
        testImageView.frame.size.width = view.frame.size.width
        testImageView.frame.size.height = CGFloat(roundf(height))
        testImageView.frame.origin.y = CGFloat(roundf(originY))
        
        // Resize ad container view
        height = Float(adContainerView.frame.size.width) / 16.0 * 9.0
        originY = Float(view.frame.size.height) / 2.0 - height / 2.0
        
        adContainerView.frame.size.width = view.frame.size.width
        adContainerView.frame.size.height = CGFloat(roundf(height))
        adContainerView.frame.origin.y = CGFloat(roundf(originY))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        startShowingAds()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        stopShowingAds()
    }
    
    @IBAction func showButtonPressed(_ sender: Any) {
        showButton.isHidden = true
        indicatorView.startAnimating()
        
        if let standaloneAd = self.standaloneAd {
            standaloneAd.load()
        }
    }
}

// MARK: - Standalone Ad

extension StandaloneAdViewController {
    
    func startShowingAds() {
        if let _ = standaloneAd {
            return
        }
        
        if FAStandaloneAd.isAvailable(forPlacementId: "standalone") {
            // Setup FAStandaloneAdConfig
            let config = FAStandaloneAdConfig()
            config.placementId = "standalone"
            
            // Setup your FAStandaloneAd instance
            standaloneAd = FAStandaloneAd(config: config)
            standaloneAd?.delegate = self
        }
    }
    
    func stopShowingAds() {
        standaloneAd?.cancel()
        standaloneAd?.adView.removeFromSuperview()
        standaloneAd?.delegate = nil
        standaloneAd = nil
    }
    
}

// MARK: - FAStandaloneAdDelegate

extension StandaloneAdViewController: FAStandaloneAdDelegate {
    
    func standaloneAd(_ standaloneAd: FAStandaloneAd, didFailWithError error: Error) {
        print("***** \(#function): standaloneAd = \(standaloneAd), error = \(error)")
        
        // Hide ad container view and spinner
        adContainerView.isHidden = true
        indicatorView.stopAnimating()
    }
    
    func standaloneAdDidChangeSize(_ standaloneAd: FAStandaloneAd) {
        print("***** \(#function): standaloneAd = \(standaloneAd)")
        
        let preferredSize = standaloneAd.size(forSuperviewSize: CGSize(width: view.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        
        adContainerView.frame.size.width = preferredSize.width
        adContainerView.frame.size.height = preferredSize.height
        adContainerView.center = view.center
    }
    
    func standaloneAdDidFinishLoading(_ standaloneAd: FAStandaloneAd) {
        print("***** \(#function): standaloneAd = \(standaloneAd)")
        
        indicatorView.stopAnimating()
        adContainerView.isHidden = false
        
        // Add ad view to our container view
        let adView = standaloneAd.adView
        adView.frame = adContainerView.bounds
        adView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        adContainerView.addSubview(adView)
    }
    
    func standaloneAdDidFinishPlaying(_ standaloneAd: FAStandaloneAd) {
        print("***** \(#function): standaloneAd = \(standaloneAd)")
        
        // Hide ad container view
        adContainerView.isHidden = true
    }
    
}

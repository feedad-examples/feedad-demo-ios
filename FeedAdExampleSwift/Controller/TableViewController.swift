//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

import UIKit
import FeedAd

class TableViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var feedAd: FAFeedAd? = nil
    var displayData = [Any]()
    var data = [NewsEntry]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        updateTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startShowingAds()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopShowingAds()
    }
    
}

// MARK: - Feed Ads

extension TableViewController {
    
    func startShowingAds() {
        if let _ = feedAd {
            return
        }

        // Setup video ad with placement id
        // The placement id may be chosen by the developer and
        // allows to distinguish different screens
        let config = FAFeedAdConfig()
        config.placementId = "table-view"
        
        feedAd = FAFeedAd(config: config)
        feedAd?.delegate = self
        feedAd?.load()
    }
    
    func stopShowingAds() {
        feedAd?.cancel()
        feedAd?.adView.removeFromSuperview()
        feedAd?.delegate = nil
        feedAd = nil
        
        updateTableView()
    }
    
}

// MARK: - Setup Data

extension TableViewController {
    
    func injectAds(_ array: [NewsEntry]) -> [Any] {
        guard let feedAd = self.feedAd, feedAd.isLoaded else {
            return data
        }
        
        let start = 2
        let gapItemCount = 7
        
        if data.count < start {
            return data
        }
        
        var injectedData = [Any]()
        injectedData.append(contentsOf: data as [Any])
        
        var i = start
        while i < injectedData.count {
            injectedData.insert(feedAd, at: i)
            i += gapItemCount
        }
        
        return injectedData
    }
    
    func loadData() {
        let newsEntry = NewsEntry()
        for _ in stride(from: 0, to: 15, by: 1) {
            data.append(newsEntry)
        }

    }
    
    func updateTableView() {
        displayData = injectAds(data)
        tableView.reloadData()
    }
    
}

// MARK: - FAFeedAdDelegate

extension TableViewController: FAFeedAdDelegate {
    
    func feedAd(_ feedAd: FAFeedAd, didFailWithError error: Error) {
        print("***** \(#function): feedAd = \(feedAd), error = \(error)")
        updateTableView()
    }
    
    func feedAdDidChangeSize(_ feedAd: FAFeedAd) {
        print("***** \(#function): feedAd = \(feedAd)")
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func feedAdDidFinishLoading(_ feedAd: FAFeedAd) {
        print("***** \(#function): feedAd = \(feedAd)")
        updateTableView()
    }
    
    func feedAdDidFinishPlaying(_ feedAd: FAFeedAd) {
        print("***** \(#function): feedAd = \(feedAd)")
        updateTableView()
    }
    
}

// MARK: - UITableViewDelegate

extension TableViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedElement = displayData[indexPath.row] as? NewsEntry {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String(describing: NewsEntryViewController.self)) as! NewsEntryViewController
            controller.entry = selectedElement
            controller.title = ""
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let currentElement = displayData[indexPath.row]
        
        if let ad = currentElement as? FAFeedAd {
            return ad.size(forSuperviewSize: CGSize(width: tableView.frame.size.width, height: CGFloat.greatestFiniteMagnitude)).height
        }
        
        return 169.0
    }
    
}

// MARK: - UITableViewDataSource

extension TableViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentElement = displayData[indexPath.row]
        
        if let ad = currentElement as? FAFeedAd {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FeedAdTableViewCell.self), for: indexPath) as! FeedAdTableViewCell
            cell.injectAdView(ad.adView)
            return cell
        }
        
        let entry = currentElement as! NewsEntry
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewsEntryTableViewCell.self), for: indexPath) as! NewsEntryTableViewCell
        cell.setup(from: entry)
        return cell
    }
    
}

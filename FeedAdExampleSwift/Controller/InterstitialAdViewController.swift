//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

import UIKit
import FeedAd


enum InterstitialAdViewControllerState {
    case unknown
    case initializing
    case initialized
    case loading
    case loaded
    case showing
}


class InterstitialAdViewController: UIViewController {
    
    var interstitialAd: FAInterstitialAd? = nil
    var state: InterstitialAdViewControllerState = .unknown {
        didSet {
            var shouldEnableLoadButton = false
            var shouldEnableShowButton = false
            
            if (state == .unknown) {
                shouldEnableLoadButton = true
            } else if (state == .initialized) {
                shouldEnableLoadButton = true
            } else if (state == .loaded) {
                shouldEnableShowButton = true
            }
            
            // activityIndicatorView
            self.activityIndicatorView.isHidden = state != .initializing && state != .loading && state != .showing
            
            // loadButton
            self.loadButton.isUserInteractionEnabled = shouldEnableLoadButton;
            self.loadButton.alpha                    = shouldEnableLoadButton ? 1.0 : 0.5
            
            // showButton
            self.showButton.isUserInteractionEnabled = shouldEnableShowButton;
            self.showButton.alpha                    = shouldEnableShowButton ? 1.0 : 0.5
        }
    }
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var loadButton: UIButton!
    @IBOutlet weak var showButton: UIButton!
    
    
    // MARK: - View Lifecycle
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        startShowingAds()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        stopShowingAds()
    }
    
    
    // MARK: - Actions
    
    @IBAction func didTapLoadButton(_ sender: Any) {
        self.startShowingAds()
    }
    
    @IBAction func didTapShowButton(_ sender: Any) {
        self.state = .showing
        
        // Show loaded interstitial ad
        self.interstitialAd?.show()
    }
}

// MARK: - Interstitial Ad

extension InterstitialAdViewController {
    
    func startShowingAds() {
        if let _ = interstitialAd {
            self.stopShowingAds()
        }
        
        if FAInterstitialAd.isAvailable(forPlacementId: "interstitial") {
            self.state = .initializing
            
            // Setup FAInterstitialAd
            let config = FAInterstitialAdConfig()
            config.placementId = "interstitial"
            
            let interstitialAd = FAInterstitialAd(config: config)
            interstitialAd.delegate = self
            self.interstitialAd = interstitialAd
            
            // Load ad
            self.state = .loading
            
            interstitialAd.load()
        }
    }
    
    func stopShowingAds() {
        interstitialAd?.cancel()
        interstitialAd?.delegate = nil
        interstitialAd = nil
        
        self.state = .unknown
    }
    
}

// MARK: - FAInterstitialAdDelegate

extension InterstitialAdViewController: FAInterstitialAdDelegate {
    
    func interstitialAd(_ interstitialAd: FAInterstitialAd, didFailWithError error: Error) {
        print("***** \(#function): interstitialAd = \(interstitialAd), error = \(error)")
        
        // Cannot reuse FAInterstitialAd, so we'll clean it up here
        self.stopShowingAds()
    }
    
    func interstitialAdDidFinishLoading(_ interstitialAd: FAInterstitialAd) {
        print("***** \(#function): interstitialAd = \(interstitialAd)")
        
        self.state = .loaded
    }
    
    func interstitialAdDidFinishPlaying(_ interstitialAd: FAInterstitialAd) {
        print("***** \(#function): interstitialAd = \(interstitialAd)")
        
        // Cannot reuse FAInterstitialAd, so we'll clean it up here
        self.stopShowingAds()
    }
    
}

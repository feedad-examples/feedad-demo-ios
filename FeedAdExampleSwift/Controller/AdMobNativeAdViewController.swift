//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

import FeedAd_GAM
import GoogleMobileAds
import UIKit


class AdMobNativeAdViewController: UIViewController {
    // Make sure to update GADApplicationIdentifier in your Info.plist
    let ADMOB_NATIVEAD_ADUNIT_ID: String = "<your-admob-adunit-id>"
    
    var admobAdIsLoaded = false
    var admobAdLoader: GADAdLoader? = nil
    var admobNativeAdView: UIView? = nil
    var displayData = [Any]()
    var data = [NewsEntry]()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        updateTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startShowingAds()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopShowingAds()
    }
}


// MARK: - AdMob Ads

extension AdMobNativeAdViewController {
    func startShowingAds() {
        if let _ = self.admobAdLoader {
            return
        }
        
        let adTypes = [ GADAdLoaderAdType.native ]
        let admobAdLoader = GADAdLoader(adUnitID: ADMOB_NATIVEAD_ADUNIT_ID, rootViewController:self, adTypes: adTypes, options: nil)
        admobAdLoader.delegate = self
        self.admobAdLoader = admobAdLoader
        
        admobAdLoader.load(GADRequest())
    }
    
    func stopShowingAds() {
        admobAdIsLoaded = false
        
        admobNativeAdView?.removeFromSuperview()
        admobNativeAdView = nil
        admobAdLoader?.delegate = nil
        admobAdLoader = nil
        
        updateTableView()
    }
}

// MARK: - Setup Data

extension AdMobNativeAdViewController {
    func injectAds(_ array: [NewsEntry]) -> [Any] {
        guard let admobNativeAdView = admobNativeAdView, admobAdIsLoaded else {
            return data
        }
        
        let start = 2
        let gapItemCount = 7
        
        if data.count < start {
            return data
        }
        
        var injectedData = [Any]()
        injectedData.append(contentsOf: data as [Any])
        
        var i = start
        while i < injectedData.count {
            injectedData.insert(admobNativeAdView, at: i)
            i += gapItemCount
        }
        
        return injectedData
    }
    
    func loadData() {
        let newsEntry = NewsEntry()
        for _ in stride(from: 0, to: 15, by: 1) {
            data.append(newsEntry)
        }
    }
    
    func updateTableView() {
        displayData = injectAds(data)
        tableView.reloadData()
    }
}

// MARK: GADNativeAdLoaderDelegate

extension AdMobNativeAdViewController: GADNativeAdLoaderDelegate {
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADNativeAd) {
        print("***** \(#function): adLoader = \(adLoader), nativeAd = \(nativeAd)")
        
        if let extraAssets = nativeAd.extraAssets, let _ = extraAssets[MediatedNativeAd.extrasIdKey] {
            let admobNativeAdViewFrame = CGRect(x: 0.0, y: 0.0, width: 320.0, height: 180.0)
            let admobNativeAdView = NativeAdView(frame: admobNativeAdViewFrame)
            admobNativeAdView.setup(withNativeAd: nativeAd)
            self.admobNativeAdView = admobNativeAdView
        } else {
            // TODO Handle other ads
            print("***** \(#function): Received an unexpected ad type")
        }
        
        if let _ = self.admobNativeAdView {
            self.admobAdIsLoaded = true
            self.updateTableView()
        }
    }
}

// MARK: GADAdLoaderDelegate

extension AdMobNativeAdViewController: GADAdLoaderDelegate {
    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: Error) {
        print("***** \(#function): adLoader = \(adLoader), error = \(error)")
        
        admobAdIsLoaded = false
        updateTableView()
    }
}

// MARK: - UITableViewDelegate

extension AdMobNativeAdViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedElement = displayData[indexPath.row] as? NewsEntry {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String(describing: NewsEntryViewController.self)) as! NewsEntryViewController
            controller.entry = selectedElement
            controller.title = ""
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let currentElement = displayData[indexPath.row]
        
        if currentElement is NativeAdView {
            let height = Float(tableView.frame.size.width) / 16.0 * 9.0
            return CGFloat(roundf(height) + 1.0)
        }
        
        return 169.0
    }
}

// MARK: - UITableViewDataSource

extension AdMobNativeAdViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentElement = displayData[indexPath.row]
        
        if let ad = currentElement as? NativeAdView {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FeedAdTableViewCell.self), for: indexPath) as! FeedAdTableViewCell
            cell.injectAdView(ad)
            return cell
        }
        
        let entry = currentElement as! NewsEntry
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewsEntryTableViewCell.self), for: indexPath) as! NewsEntryTableViewCell
        cell.setup(from: entry)
        return cell
    }
}

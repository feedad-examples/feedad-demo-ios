//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

import UIKit
import FeedAd

class NewsEntryViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var entry: NewsEntry!
    var feedAd: FAFeedAd? = nil
    var data = [Any]()
    var displayData = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        updateTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startShowingAds()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopShowingAds()
    }
}

// MARK: - Feed Ads

extension NewsEntryViewController {
    
    func startShowingAds() {
        if let _ = feedAd {
            return
        }
        
        // Setup video ad with placement id
        // The placement id may be chosen by the developer and
        // allows to distinguish different screens
        let config = FAFeedAdConfig()
        config.placementId = "news-entry-detail"
        
        feedAd = FAFeedAd(config: config)
        feedAd?.delegate = self
        feedAd?.load()
    }
    
    func stopShowingAds() {
        feedAd?.cancel()
        feedAd?.adView.removeFromSuperview()
        feedAd?.delegate = nil
        feedAd = nil
        
        updateTableView()
    }
}

// MARK: - Setup Data

extension NewsEntryViewController {
    
    func injectAds(_ array: [Any]) -> [Any] {
        guard let feedAd = self.feedAd, feedAd.isLoaded else {
            return data
        }
        
        let start = 2
        let gapItemCount = 5
        
        if data.count < start {
            return data
        }
        
        var injectedData = [Any]()
        injectedData.append(contentsOf: data as [Any])
        
        var i = start
        while i < injectedData.count {
            injectedData.insert(feedAd, at: i)
            i += gapItemCount
        }
        
        return injectedData
    }
    
    func loadData() {
        guard let entry = self.entry else {
            return
        }
        
        data.append(entry)
        let paragraphs = entry.body.components(separatedBy: "\n\n")
        data.append(contentsOf: paragraphs as [Any])
    }
    
    func updateTableView() {
        displayData = injectAds(data)
        tableView.reloadData()
    }
    
}

// MARK: - FAFeedAdDelegate

extension NewsEntryViewController: FAFeedAdDelegate {
    
    func feedAd(_ feedAd: FAFeedAd, didFailWithError error: Error) {
        print("***** \(#function): feedAd = \(feedAd), error = \(error)")
        updateTableView()
    }
    
    func feedAdDidChangeSize(_ feedAd: FAFeedAd) {
        print("***** \(#function): feedAd = \(feedAd)")
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func feedAdDidFinishLoading(_ feedAd: FAFeedAd) {
        print("***** \(#function): feedAd = \(feedAd)")
        updateTableView()
    }
    
    func feedAdDidFinishPlaying(_ feedAd: FAFeedAd) {
        print("***** \(#function): feedAd = \(feedAd)")
        updateTableView()
    }
    
}

// MARK: - UITableViewDelegate 

extension NewsEntryViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let currentElement = displayData[indexPath.row]
        
        if let ad = currentElement as? FAFeedAd {
            return ad.size(forSuperviewSize: CGSize(width: tableView.frame.size.width, height: CGFloat.greatestFiniteMagnitude)).height
        }
        
        if let paragraph = currentElement as? String {
            return NewsEntryParagraphTableViewCell.heightForParagraph(paragraph, withWidth: tableView.frame.size.width)
        }
        
        return 90.0
    }
    
}

// MARK: - UITableViewDataSource

extension NewsEntryViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentElement = displayData[indexPath.row]
        
        if let ad = currentElement as? FAFeedAd {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FeedAdTableViewCell.self), for: indexPath) as! FeedAdTableViewCell
            cell.injectAdView(ad.adView)
            return cell
        }
        
        if let entry = currentElement as? NewsEntry {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewsEntryTitlesTableViewCell.self), for: indexPath) as! NewsEntryTitlesTableViewCell
            
            cell.setup(from: entry)
            return cell
        }
        
        let paragraph = currentElement as! String
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewsEntryParagraphTableViewCell.self), for: indexPath) as! NewsEntryParagraphTableViewCell
        
        cell.setup(from: paragraph)
        return cell
    }
    
}

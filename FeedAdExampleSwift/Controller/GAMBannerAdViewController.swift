//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

import UIKit
import GoogleMobileAds


class GAMBannerAdViewController: UIViewController {
    // Make sure to update GADApplicationIdentifier in your Info.plist
    let GAM_BANNERAD_ADUNIT_ID: String = "<your-gam-adunit-id>"
    
    var displayData = [Any]()
    var data = [NewsEntry]()
    var gamAdIsLoaded = false
    var gamAdLoader: GADAdLoader? = nil
    var gamBannerView: GAMBannerView? = nil
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        updateTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startShowingAds()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopShowingAds()
    }
}


// MARK: - GAM Ads

extension GAMBannerAdViewController {
    func startShowingAds() {
        if self.gamBannerView != nil {
            return
        }
        
        let gamBannerView = GAMBannerView(adSize: GADAdSizeFullWidthPortraitWithHeight(250.0))
        gamBannerView.adUnitID = GAM_BANNERAD_ADUNIT_ID
        gamBannerView.delegate = self
        gamBannerView.rootViewController = self
        self.gamBannerView = gamBannerView
        
        gamBannerView.load(GADRequest())
    }
    
    func stopShowingAds() {
        self.gamAdIsLoaded = false
        
        self.gamBannerView?.removeFromSuperview()
        self.gamBannerView?.delegate = nil
        self.gamBannerView = nil
        
        self.updateTableView()
    }
}

// MARK: - Setup Data

extension GAMBannerAdViewController {
    func injectAds(_ array: [NewsEntry]) -> [Any] {
        guard let gamBannerView = self.gamBannerView, self.gamAdIsLoaded else {
            return self.data
        }
        
        let start = 2
        let gapItemCount = 7
        
        if self.data.count < start {
            return self.data
        }
        
        var injectedData = [Any]()
        injectedData.append(contentsOf: self.data as [Any])
        
        var i = start
        while i < injectedData.count {
            injectedData.insert(gamBannerView, at: i)
            i += gapItemCount
        }
        
        return injectedData
    }
    
    func loadData() {
        let newsEntry = NewsEntry()
        for _ in stride(from: 0, to: 15, by: 1) {
            self.data.append(newsEntry)
        }
    }
    
    func updateTableView() {
        self.displayData = injectAds(self.data)
        self.tableView.reloadData()
    }
}


// MARK: - GADBannerViewDelegate

extension GAMBannerAdViewController: GADBannerViewDelegate {
    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("***** \(#function): bannerView = \(bannerView)")
        
        self.gamAdIsLoaded = true
        self.updateTableView()
    }
    
    func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: any Error) {
        print("***** \(#function): bannerView = \(bannerView), error = \(error)")
        
        self.gamAdIsLoaded = false
        self.updateTableView()
    }
}


// MARK: - UITableViewDelegate

extension GAMBannerAdViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedElement = self.displayData[indexPath.row] as? NewsEntry {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = mainStoryboard.instantiateViewController(withIdentifier: String(describing: NewsEntryViewController.self)) as! NewsEntryViewController
            controller.entry = selectedElement
            controller.title = ""
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let currentElement = displayData[indexPath.row]
        
        if currentElement is GAMBannerView {
            let height = Float(tableView.frame.size.width) / 16.0 * 9.0
            return CGFloat(roundf(height) + 1.0)
        }
        
        return 169.0
    }
}


// MARK: - UITableViewDataSource

extension GAMBannerAdViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.displayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentElement = self.displayData[indexPath.row]
        
        if let ad = currentElement as? GAMBannerView {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FeedAdTableViewCell.self), for: indexPath) as! FeedAdTableViewCell
            cell.injectAdView(ad)
            return cell
        }
        
        let entry = currentElement as! NewsEntry
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewsEntryTableViewCell.self), for: indexPath) as! NewsEntryTableViewCell
        cell.setup(from: entry)
        return cell
    }
}

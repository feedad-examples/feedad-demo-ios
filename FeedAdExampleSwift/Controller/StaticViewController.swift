//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

import UIKit
import FeedAd

class StaticViewController: UIViewController {
    
    @IBOutlet weak var adContainerView: UIView!
    var feedAd: FAFeedAd? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Resize ad container view
        let height = Float(adContainerView.frame.size.width) / 16.0 * 9.0
        let originY = Float(view.frame.size.height) / 2.0 - height / 2.0
        
        adContainerView.frame.size.width = view.frame.size.width
        adContainerView.frame.size.height = CGFloat(roundf(height))
        adContainerView.frame.origin.y = CGFloat(roundf(originY))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startShowingAds()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        stopShowingAds()
    }

}

// MARK: - Feed Ads

extension StaticViewController {
    func startShowingAds() {
        if let _ = feedAd {
            return
        }

        // Setup FAFeedAdConfig
        let config = FAFeedAdConfig()
        config.placementId = "static"
        
        // Setup your FAFeedAd instance
        feedAd = FAFeedAd(config: config)
        feedAd?.delegate = self
        feedAd?.load()
    }
    
    func stopShowingAds() {
        feedAd?.cancel()
        feedAd?.adView.removeFromSuperview()
        feedAd?.delegate = nil
        feedAd = nil
    }
}

// MARK: - FAFeedAdDelegate

extension StaticViewController: FAFeedAdDelegate {
    
    func feedAd(_ feedAd: FAFeedAd, didFailWithError error: Error) {
        print("***** \(#function): feedAd = \(feedAd), error = \(error)")
    }
    
    func feedAdDidChangeSize(_ feedAd: FAFeedAd) {
        print("***** \(#function): feedAd = \(feedAd)")
        
        let preferredSize = feedAd.size(forSuperviewSize: CGSize(width: view.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        
        adContainerView.frame.size.width = preferredSize.width
        adContainerView.frame.size.height = preferredSize.height
        adContainerView.center = view.center
    }
    
    func feedAdDidFinishLoading(_ feedAd: FAFeedAd) {
        print("***** \(#function): feedAd = \(feedAd)")
        
        // Add ad view to our container view
        let adView = feedAd.adView
        adView.frame = adContainerView.bounds
        adContainerView.addSubview(adView)
    }
    
    func feedAdDidFinishPlaying(_ feedAd: FAFeedAd) {
        print("***** \(#function): feedAd = \(feedAd)")
    }
    
}

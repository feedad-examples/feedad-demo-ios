//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

struct NewsEntry {
    let avatarImageName = "avatar"
    let subtitle = "More Infos: FeedAd.com"
    let title = "Bjoern Exampleson"
    let body = "Nunquam visum lura. Nutrixs resistere, tanquam festus demissio. Heu, fluctus! Spatiis ridetis in lotus alta muta! Adiurators mori! Peregrinationes etiam ducunt ad altus buxum. Cum tumultumque cadunt, omnes bromiumes perdere ferox, audax cedriumes.\n\nNunquam visum lura. Nutrixs resistere, tanquam festus demissio. Heu, fluctus! Spatiis ridetis in lotus alta muta! Adiurators mori! Peregrinationes etiam ducunt ad altus buxum. Cum tumultumque cadunt, omnes bromiumes perdere ferox, audax cedriumes.\n\nNunquam visum lura. Nutrixs resistere, tanquam festus demissio. Heu, fluctus! Spatiis ridetis in lotus alta muta! Adiurators mori! Peregrinationes etiam ducunt ad altus buxum. Cum tumultumque cadunt, omnes bromiumes perdere ferox, audax cedriumes.\n\nNunquam visum lura. Nutrixs resistere, tanquam festus demissio. Heu, fluctus! Spatiis ridetis in lotus alta muta! Adiurators mori! Peregrinationes etiam ducunt ad altus buxum. Cum tumultumque cadunt, omnes bromiumes perdere ferox, audax cedriumes.\n\nNunquam visum lura. Nutrixs resistere, tanquam festus demissio. Heu, fluctus! Spatiis ridetis in lotus alta muta! Adiurators mori! Peregrinationes etiam ducunt ad altus buxum. Cum tumultumque cadunt, omnes bromiumes perdere ferox, audax cedriumes.\n\nNunquam visum lura. Nutrixs resistere, tanquam festus demissio. Heu, fluctus! Spatiis ridetis in lotus alta muta! Adiurators mori! Peregrinationes etiam ducunt ad altus buxum. Cum tumultumque cadunt, omnes bromiumes perdere ferox, audax cedriumes.\n\nNunquam visum lura. Nutrixs resistere, tanquam festus demissio. Heu, fluctus! Spatiis ridetis in lotus alta muta! Adiurators mori! Peregrinationes etiam ducunt ad altus buxum. Cum tumultumque cadunt, omnes bromiumes perdere ferox, audax cedriumes.\n\nNunquam visum lura. Nutrixs resistere, tanquam festus demissio. Heu, fluctus! Spatiis ridetis in lotus alta muta! Adiurators mori! Peregrinationes etiam ducunt ad altus buxum. Cum tumultumque cadunt, omnes bromiumes perdere ferox, audax cedriumes.\n\nNunquam visum lura. Nutrixs resistere, tanquam festus demissio. Heu, fluctus! Spatiis ridetis in lotus alta muta! Adiurators mori! Peregrinationes etiam ducunt ad altus buxum. Cum tumultumque cadunt, omnes bromiumes perdere ferox, audax cedriumes."
}

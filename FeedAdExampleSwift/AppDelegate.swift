//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var locationManager: CLLocationManager?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Setup cofig
        let config = FAConfig()
        config.clientToken = FAGlobals.ClientToken
        
        // Send user data for user value tracking
        // and campaign targeting
        //
        // Reconfigure FAManager anytime this
        // data should be updated, e.g. after
        // logins and logouts
        //config.userAge = 24
        //config.userGender = FAGender.male
        //config.userId = "some-userid"
        
        FAManager.shared().configure(config)
        
        // Send geolocation data for
        // user location tracking
        //
        // Call +[FAManager setGeoLocation:] anytime
        // the user's location has changed
        //locationManager = nil
        //locationManager?.delegate = self
        //locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        //locationManager?.distanceFilter = 1000
        //locationManager?.requestWhenInUseAuthorization()
        
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        if let locationManager = locationManager {
            locationManager.stopUpdatingLocation()
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        if let locationManager = locationManager {
            locationManager.startUpdatingLocation()
        }
    }
    
}

// MARK: - CLLocationManagerDelegate

extension AppDelegate: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            if let err = FAManager.shared().setGeoLocation(location) {
                print("***** \(#function): err = \(err)")
            }
        }
    }
    
}

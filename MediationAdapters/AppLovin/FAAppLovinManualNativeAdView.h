//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import <AppLovinSDK/AppLovinSDK.h>
#import <FeedAd_AppLovin/FeedAd_AppLovin.h>


NS_ASSUME_NONNULL_BEGIN

@interface FAAppLovinManualNativeAdView : MANativeAdView <FAAppLovinManualNativeAdLayouter>

@property (nonatomic, weak) IBOutlet UIView *feedAdMediaContentView;


/// Adapts the native ad layout for full-height and -width display of the FeedAd view
///
/// Make sure to customize this to your needs
- (void)layoutForFeedAd;

@end

NS_ASSUME_NONNULL_END

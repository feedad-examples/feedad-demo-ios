//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FAAppLovinManualNativeAdView.h"


@implementation FAAppLovinManualNativeAdView

- (void)layoutForFeedAd {
    // Show FeedAd media content view
    self.feedAdMediaContentView.hidden = NO;
    
    // Move children from default media content view to FeedAd content view
    for (UIView *subview in self.mediaContentView.subviews) {
        [self.feedAdMediaContentView addSubview:subview];
        
        [NSLayoutConstraint activateConstraints:@[
            [NSLayoutConstraint constraintWithItem:subview attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.feedAdMediaContentView attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f],
            [NSLayoutConstraint constraintWithItem:subview attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.feedAdMediaContentView attribute:NSLayoutAttributeWidth multiplier:1.0f constant:0.0f],
            [NSLayoutConstraint constraintWithItem:subview attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.feedAdMediaContentView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f],
            [NSLayoutConstraint constraintWithItem:subview attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.feedAdMediaContentView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f],
        ]];
    }
}

@end

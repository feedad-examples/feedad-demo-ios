//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FANewsEntryTableViewCell.h"


@interface FANewsEntryTableViewCell ()

@property (nonatomic, weak) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, weak) IBOutlet UILabel     *bodyLabel;
@property (nonatomic, weak) IBOutlet UILabel     *subtitleLabel;
@property (nonatomic, weak) IBOutlet UILabel     *titleLabel;

@end


@implementation FANewsEntryTableViewCell

- (void)setupFromNewsEntry:(FANewsEntry *)entry {
    self.avatarImageView.image = [UIImage imageNamed:entry.avatarImageName];
    self.bodyLabel.text        = entry.body;
    self.subtitleLabel.text    = entry.subtitle;
    self.titleLabel.text       = entry.title;
}

@end

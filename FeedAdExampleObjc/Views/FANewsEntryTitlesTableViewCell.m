//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FANewsEntryTitlesTableViewCell.h"


@interface FANewsEntryTitlesTableViewCell ()

@property (nonatomic, weak) IBOutlet UIImageView *avatarImageView;
@property (nonatomic, weak) IBOutlet UILabel     *subtitleLabel;
@property (nonatomic, weak) IBOutlet UILabel     *titleLabel;

@end


@implementation FANewsEntryTitlesTableViewCell

- (void)setupFromNewsEntry:(FANewsEntry *)entry {
    self.avatarImageView.image = [UIImage imageNamed:entry.avatarImageName];
    self.subtitleLabel.text    = entry.subtitle;
    self.titleLabel.text       = entry.title;
}

@end

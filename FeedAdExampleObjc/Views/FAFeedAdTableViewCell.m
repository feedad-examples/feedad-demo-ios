//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FAFeedAdTableViewCell.h"
#import <GoogleMobileAds/GoogleMobileAds.h>


@interface FAFeedAdTableViewCell ()

@property (nonatomic, weak) IBOutlet UIView *containerView;

@end


@implementation FAFeedAdTableViewCell

- (void)prepareForReuse {
    [super prepareForReuse];
    
    for (UIView *subview in self.containerView.subviews) {
        [subview removeFromSuperview];
    }
}

- (void)injectAdView:(UIView *)adView {
    if ([adView isKindOfClass:[GAMBannerView class]]) {
        // We must use [GAMBannerView resize:] whenever its
        // size has to change - otherwise it will simply
        // cancel the display of the ad
        [(GAMBannerView *)adView resize:GADAdSizeFromCGSize(self.containerView.bounds.size)];
    } else {
        adView.frame            = self.containerView.bounds;
        adView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    }

    [self.containerView addSubview:adView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (self.containerView.subviews.count > 0) {
        UIView *adView = self.containerView.subviews[0];
        if ([adView isKindOfClass:[GAMBannerView class]]) {
            // We must use [GAMBannerView resize:] whenever its
            // size has to change - otherwise it will simply
            // cancel the display of the ad
            [(GAMBannerView *)adView resize:GADAdSizeFromCGSize(self.containerView.bounds.size)];
        }
    }
}

@end

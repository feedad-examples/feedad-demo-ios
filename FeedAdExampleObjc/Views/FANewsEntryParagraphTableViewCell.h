//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import <UIKit/UIKit.h>


@interface FANewsEntryParagraphTableViewCell : UITableViewCell

+ (CGFloat)heightForParagraph:(NSString *)paragraph width:(CGFloat)width;

- (void)setupFromParagraph:(NSString *)paragraph;

@end

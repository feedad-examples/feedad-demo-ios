//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FAFeedAdCollectionViewCell.h"


@interface FAFeedAdCollectionViewCell ()

@property (nonatomic, weak) IBOutlet UIView *containerView;

@end


@implementation FAFeedAdCollectionViewCell

- (void)prepareForReuse {
    [super prepareForReuse];
    
    for (UIView *subview in self.containerView.subviews) {
        [subview removeFromSuperview];
    }
}

- (void)injectAdView:(UIView *)adView {
    adView.frame            = self.containerView.bounds;
    adView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.containerView addSubview:adView];
}

@end

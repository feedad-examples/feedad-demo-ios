//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FANewsEntryParagraphTableViewCell.h"


const CGFloat FANewsEntryParagraphTableViewCellHorizontalMargin = 8.0f * 2.0f;
const CGFloat FANewsEntryParagraphTableViewCellVerticalMargin   = 8.0f * 2.0f;


@interface FANewsEntryParagraphTableViewCell ()

@property (nonatomic, strong) IBOutlet UILabel *paragraphLabel;

@end


@implementation FANewsEntryParagraphTableViewCell

+ (CGFloat)heightForParagraph:(NSString *)paragraph width:(CGFloat)width {
    NSDictionary *attributes = @{ NSFontAttributeName : [UIFont systemFontOfSize:16.0f] };
    CGSize boundingSize      = CGSizeMake(width - FANewsEntryParagraphTableViewCellVerticalMargin, CGFLOAT_MAX);
    CGRect boundingRect      = [paragraph boundingRectWithSize:boundingSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    return ceilf(boundingRect.size.height) + FANewsEntryParagraphTableViewCellHorizontalMargin;
}



- (void)setupFromParagraph:(NSString *)paragraph {
    self.paragraphLabel.text = paragraph;
}

@end

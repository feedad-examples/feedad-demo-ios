//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import <CoreLocation/CoreLocation.h>
#import "FAAppDelegate.h"
#import "FAGlobals.h"
#import <FeedAd/FeedAd.h>


@interface FAAppDelegate () <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;

@end


@implementation FAAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Setup config with publisher token
    FAConfig *config = [FAConfig new];
    config.clientToken = FA_CLIENT_TOKEN;
    
    // Send user data for user value tracking
    // and campaign targeting
    //
    // Reconfigure FAManager anytime this
    // data should be updated, e.g. after
    // logins and logouts
    //config.userAge = 24;
    //config.userGender = FAGenderMale;
    //config.userId = @"some-userid";
    
    FAManager *manager = [FAManager sharedManager];
    [manager configure:config];
    
    // Send geolocation data for
    // user location tracking
    //
    // Call +[FAManager setGeoLocation:] anytime
    // the user's location has changed
    //self.locationManager                 = [CLLocationManager new];
    //self.locationManager.delegate        = self;
    //self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    //self.locationManager.distanceFilter  = 1000.0f;
    //[self.locationManager requestWhenInUseAuthorization];
    
    return YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    if (self.locationManager) {
        [self.locationManager stopUpdatingLocation];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    if (self.locationManager) {
        [self.locationManager startUpdatingLocation];
    }
}


#pragma mark CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    CLLocation *location = locations.lastObject;
    if (locations) {
        [[FAManager sharedManager] setGeoLocation:location]; 
    }
}

@end

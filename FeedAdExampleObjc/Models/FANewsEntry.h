//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import <Foundation/Foundation.h>


@interface FANewsEntry : NSObject

@property (nonatomic, strong) NSString *avatarImageName;
@property (nonatomic, strong) NSString *body;
@property (nonatomic, strong) NSString *subtitle;
@property (nonatomic, strong) NSString *title;


+ (instancetype)dummyEntry;

@end

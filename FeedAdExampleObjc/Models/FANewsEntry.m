//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FANewsEntry.h"


@implementation FANewsEntry

+ (instancetype)dummyEntry {
    FANewsEntry *entry    = [FANewsEntry new];
    entry.avatarImageName = @"avatar";
    entry.subtitle        = @"More Infos: FeedAd.com";
    entry.title           = @"Bjoern Exampleson";
    
    NSString *part        = @"Nunquam visum lura. Nutrixs resistere, tanquam festus demissio. Heu, fluctus! Spatiis ridetis in lotus alta muta! Adiurators mori! Peregrinationes etiam ducunt ad altus buxum. Cum tumultumque cadunt, omnes bromiumes perdere ferox, audax cedriumes.";
    NSMutableArray *parts = [NSMutableArray array];
    for (NSInteger i = 0; i < 9; i++) {
        [parts addObject:part];
    }
    entry.body = [parts componentsJoinedByString:@"\n\n"];
    
    return entry;
}

@end

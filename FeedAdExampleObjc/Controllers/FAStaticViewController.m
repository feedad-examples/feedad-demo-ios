//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FAStaticViewController.h"
#import <FeedAd/FeedAd.h>
#import "FAGlobals.h"


@interface FAStaticViewController () <FAFeedAdDelegate>

@property (nonatomic, weak)   IBOutlet UIView *adContainerView;
@property (nonatomic, strong) FAFeedAd        *feedAd;

@end


@implementation FAStaticViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Resize ad container view
    self.adContainerView.frame = ({
        CGRect frame      = CGRectZero;
        frame.size.width  = self.view.frame.size.width;
        frame.size.height = roundf(frame.size.width / 16.0f * 9.0f);
        frame.origin.y    = roundf(self.view.frame.size.height / 2.0f - frame.size.height / 2.0f);
        frame;
    });
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self startShowingAds];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self stopShowingAds];
}


#pragma mark - Feed Ads

- (void)startShowingAds {
    if (self.feedAd) {
        return;
    }

    // Setup FAFeedAdConfig
    FAFeedAdConfig *config = [FAFeedAdConfig new];
    config.placementId = @"static";
    
    // Setup your FAFeedAd instance
    self.feedAd = [[FAFeedAd alloc] initWithConfig:config];
    self.feedAd.delegate = self;
    
    // Load ad
    [self.feedAd load];
}

- (void)stopShowingAds {
    [self.feedAd cancel];
    [self.feedAd.adView removeFromSuperview];
    self.feedAd.delegate = nil;
    self.feedAd = nil;
}
 

#pragma mark FAFeedAdDelegate
    
- (void)feedAd:(FAFeedAd *)feedAd didFailWithError:(NSError *)error {
    NSLog(@"***** %s: feedAd = %@, error = %@", __FUNCTION__, feedAd, error);
}
    
- (void)feedAdDidFinishLoading:(FAFeedAd *)feedAd {
    NSLog(@"***** %s: feedAd = %@", __FUNCTION__, feedAd);
    
    // Add ad view to our container view
    UIView *adView          = self.feedAd.adView;
    adView.frame            = self.adContainerView.bounds;
    [self.adContainerView addSubview:adView];
}

- (void)feedAdDidFinishPlaying:(FAFeedAd *)feedAd {
    NSLog(@"***** %s: feedAd = %@", __FUNCTION__, feedAd);
}

- (void)feedAdDidChangeSize:(FAFeedAd *)feedAd {
    NSLog(@"***** %s: feedAd = %@", __FUNCTION__, feedAd);
    
    CGSize preferredSize = [feedAd sizeForSuperviewSize:CGSizeMake(self.view.frame.size.width, CGFLOAT_MAX)];
    
    self.adContainerView.frame = ({
        CGRect frame = self.adContainerView.frame;
        frame.size.width  = preferredSize.width;
        frame.size.height = preferredSize.height;
        frame;
    });
    
    self.adContainerView.center = self.view.center;
}

@end

//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FAAdMobInterstitialAdViewController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "FAGLobals.h"


// Make sure to update GADApplicationIdentifier in your Info.plist
#define ADMOB_INTERSTITIALAD_ADUNIT_ID @"<your-admob-adunit-id>"


typedef NS_ENUM(NSInteger, FAAdMobInterstitialAdViewControllerState) {
    FAAdMobInterstitialAdViewControllerStateInitializing,
    FAAdMobInterstitialAdViewControllerStateInitialized,
    FAAdMobInterstitialAdViewControllerStateLoading,
    FAAdMobInterstitialAdViewControllerStateLoaded,
    FAAdMobInterstitialAdViewControllerStateShowing
};


@interface FAAdMobInterstitialAdViewController () <GADFullScreenContentDelegate>

@property (nonatomic, weak)   IBOutlet UIActivityIndicatorView         *activityIndicatorView;
@property (nonatomic, strong) GADInterstitialAd                        *adMobInterstitialAd;
@property (nonatomic, weak)   IBOutlet UIButton                        *loadButton;
@property (nonatomic, weak)   IBOutlet UIButton                        *showButton;
@property (nonatomic, assign) FAAdMobInterstitialAdViewControllerState  state;

@end


@implementation FAAdMobInterstitialAdViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.state = FAAdMobInterstitialAdViewControllerStateInitialized;
}


#pragma mark - Actions

- (IBAction)didTapLoadButton:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    self.state = FAAdMobInterstitialAdViewControllerStateLoading;
    
    // Load interstitial ad
    if (self.adMobInterstitialAd) {
        return;
    }
    
    self.state = FAAdMobInterstitialAdViewControllerStateLoading;
    
    GADRequest *request = [GADRequest request];
    [GADInterstitialAd loadWithAdUnitID:ADMOB_INTERSTITIALAD_ADUNIT_ID request:request completionHandler:^(GADInterstitialAd * _Nullable interstitialAd, NSError * _Nullable error) {
        NSLog(@"***** %s: interstitialAd = %@, error = %@", __FUNCTION__, interstitialAd, error);
        
        if (error) {
            return;
        }
        
        typeof(self) strongSelf = weakSelf;
        interstitialAd.fullScreenContentDelegate = strongSelf;
        strongSelf.adMobInterstitialAd = interstitialAd;
        strongSelf.state = FAAdMobInterstitialAdViewControllerStateLoaded;
    }];
}

- (IBAction)didTapShowButton:(id)sender {
    self.state = FAAdMobInterstitialAdViewControllerStateShowing;
    
    // Show loaded interstitial ad
    [self.adMobInterstitialAd presentFromRootViewController:self];
}


#pragma mark States

- (void)setState:(FAAdMobInterstitialAdViewControllerState)state {
    BOOL shouldEnableLoadButton = NO;
    BOOL shouldEnableShowButton = NO;
    
    if (state == FAAdMobInterstitialAdViewControllerStateInitialized) {
        shouldEnableLoadButton = YES;
    } else if (state == FAAdMobInterstitialAdViewControllerStateLoaded) {
        shouldEnableShowButton = YES;
    }
    
    // activityIndicatorView
    self.activityIndicatorView.hidden = state != FAAdMobInterstitialAdViewControllerStateInitializing && state != FAAdMobInterstitialAdViewControllerStateLoading && state != FAAdMobInterstitialAdViewControllerStateShowing;
    
    // loadButton
    self.loadButton.userInteractionEnabled = shouldEnableLoadButton;
    self.loadButton.alpha                  = shouldEnableLoadButton ? 1.0f : 0.5f;
    
    // showButton
    self.showButton.userInteractionEnabled = shouldEnableShowButton;
    self.showButton.alpha                  = shouldEnableShowButton ? 1.0f : 0.5f;
    
    _state = state;
}


#pragma mark - GADFullScreenContentDelegate

- (void)ad:(nonnull id<GADFullScreenPresentingAd>)ad didFailToPresentFullScreenContentWithError:(nonnull NSError *)error {
    NSLog(@"***** %s: ad = %@, error = %@", __FUNCTION__, ad, error);
    
    self.state = FAAdMobInterstitialAdViewControllerStateInitialized;
}

- (void)adDidDismissFullScreenContent:(nonnull id<GADFullScreenPresentingAd>)ad {
    NSLog(@"***** %s: ad = %@", __FUNCTION__, ad);
    
    self.adMobInterstitialAd = nil;
    self.state = FAAdMobInterstitialAdViewControllerStateInitialized;
}

- (void)adWillPresentFullScreenContent:(nonnull id<GADFullScreenPresentingAd>)ad {
    NSLog(@"***** %s: ad = %@", __FUNCTION__, ad);
    
    self.state = FAAdMobInterstitialAdViewControllerStateShowing;
}

@end

//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FAGAMInterstitialAdViewController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "FAGLobals.h"


// Make sure to update GADApplicationIdentifier in your Info.plist
#define GAM_INTERSTITIALAD_ADUNIT_ID @"<your-admob-adunit-id>"


typedef NS_ENUM(NSInteger, FAGAMInterstitialAdViewControllerState) {
    FAGAMInterstitialAdViewControllerStateInitializing,
    FAGAMInterstitialAdViewControllerStateInitialized,
    FAGAMInterstitialAdViewControllerStateLoading,
    FAGAMInterstitialAdViewControllerStateLoaded,
    FAGAMInterstitialAdViewControllerStateShowing
};


@interface FAGAMInterstitialAdViewController () <GADFullScreenContentDelegate>

@property (nonatomic, weak)   IBOutlet UIActivityIndicatorView         *activityIndicatorView;
@property (nonatomic, strong) GAMInterstitialAd                        *gamInterstitialAd;
@property (nonatomic, weak)   IBOutlet UIButton                        *loadButton;
@property (nonatomic, weak)   IBOutlet UIButton                        *showButton;
@property (nonatomic, assign) FAGAMInterstitialAdViewControllerState  state;

@end


@implementation FAGAMInterstitialAdViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.state = FAGAMInterstitialAdViewControllerStateInitialized;
}


#pragma mark - Actions

- (IBAction)didTapLoadButton:(id)sender {
    __weak typeof(self) weakSelf = self;
    
    self.state = FAGAMInterstitialAdViewControllerStateLoading;
    
    // Load interstitial ad
    if (self.gamInterstitialAd) {
        return;
    }
    
    self.state = FAGAMInterstitialAdViewControllerStateLoading;
    
    GAMRequest *request = [GAMRequest request];
    [GAMInterstitialAd loadWithAdManagerAdUnitID:GAM_INTERSTITIALAD_ADUNIT_ID request:request completionHandler:^(GAMInterstitialAd * _Nullable interstitialAd, NSError * _Nullable error) {
        NSLog(@"***** %s: interstitialAd = %@, error = %@", __FUNCTION__, interstitialAd, error);
        
        if (error) {
            return;
        }
        
        typeof(self) strongSelf = weakSelf;
        interstitialAd.fullScreenContentDelegate = strongSelf;
        strongSelf.gamInterstitialAd = interstitialAd;
        strongSelf.state = FAGAMInterstitialAdViewControllerStateLoaded;
    }];
}

- (IBAction)didTapShowButton:(id)sender {
    self.state = FAGAMInterstitialAdViewControllerStateShowing;
    
    // Show loaded interstitial ad
    [self.gamInterstitialAd presentFromRootViewController:self];
}


#pragma mark States

- (void)setState:(FAGAMInterstitialAdViewControllerState)state {
    BOOL shouldEnableLoadButton = NO;
    BOOL shouldEnableShowButton = NO;
    
    if (state == FAGAMInterstitialAdViewControllerStateInitialized) {
        shouldEnableLoadButton = YES;
    } else if (state == FAGAMInterstitialAdViewControllerStateLoaded) {
        shouldEnableShowButton = YES;
    }
    
    // activityIndicatorView
    self.activityIndicatorView.hidden = state != FAGAMInterstitialAdViewControllerStateInitializing && state != FAGAMInterstitialAdViewControllerStateLoading && state != FAGAMInterstitialAdViewControllerStateShowing;
    
    // loadButton
    self.loadButton.userInteractionEnabled = shouldEnableLoadButton;
    self.loadButton.alpha                  = shouldEnableLoadButton ? 1.0f : 0.5f;
    
    // showButton
    self.showButton.userInteractionEnabled = shouldEnableShowButton;
    self.showButton.alpha                  = shouldEnableShowButton ? 1.0f : 0.5f;
    
    _state = state;
}


#pragma mark - GADFullScreenContentDelegate

- (void)ad:(nonnull id<GADFullScreenPresentingAd>)ad didFailToPresentFullScreenContentWithError:(nonnull NSError *)error {
    NSLog(@"***** %s: ad = %@, error = %@", __FUNCTION__, ad, error);
    
    self.state = FAGAMInterstitialAdViewControllerStateInitialized;
}

- (void)adDidDismissFullScreenContent:(nonnull id<GADFullScreenPresentingAd>)ad {
    NSLog(@"***** %s: ad = %@", __FUNCTION__, ad);
    
    self.gamInterstitialAd = nil;
    self.state = FAGAMInterstitialAdViewControllerStateInitialized;
}

- (void)adWillPresentFullScreenContent:(nonnull id<GADFullScreenPresentingAd>)ad {
    NSLog(@"***** %s: ad = %@", __FUNCTION__, ad);
    
    self.state = FAGAMInterstitialAdViewControllerStateShowing;
}

@end

//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import <FeedAd_GAM/FeedAd_GAM-Swift.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "FAAdMobNativeAdViewController.h"
#import "FAFeedAdTableViewCell.h"
#import "FAGlobals.h"
#import "FANewsEntry.h"
#import "FANewsEntryTableViewCell.h"
#import "FANewsEntryViewController.h"


// Make sure to update GADApplicationIdentifier in your Info.plist
#define ADMOB_NATIVEAD_ADUNIT_ID @"<your-admob-adunit-id>"


@interface FAAdMobNativeAdViewController () <GADNativeAdLoaderDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) BOOL                  admobAdIsLoaded;
@property (nonatomic, strong) GADAdLoader          *admobAdLoader;
@property (nonatomic, strong) UIView               *admobNativeAdView;
@property (nonatomic, strong) NSMutableArray       *displayedData;
@property (nonatomic, strong) NSMutableArray       *data;
@property (nonatomic, weak)   IBOutlet UITableView *tableView;

@end


@implementation FAAdMobNativeAdViewController

#pragma mark - Data

- (NSMutableArray *)injectAd:(NSMutableArray *)data {
    if (!self.admobAdIsLoaded) {
        return data;
    }
    
    NSInteger start        = 2;
    NSInteger gapItemCount = 7;
    
    if (!data || data.count < start) {
        return data;
    }
    
    data = [data mutableCopy];
    
    for (NSInteger i = start; i < data.count; i += gapItemCount) {
        [data insertObject:self.admobNativeAdView atIndex:i];
    }
    
    return data;
}

- (void)loadData {
    FANewsEntry *entry = [FANewsEntry dummyEntry];
    
    NSMutableArray *data = [NSMutableArray array];
    
    for (NSInteger i = 0; i < 15; i++) {
        [data addObject:entry];
    }
    
    self.data = [self injectAd:data];
}

- (void)updateTableView {
    self.displayedData = [self injectAd:self.data];
    
    [self.tableView reloadData];
}


#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadData];
    [self updateTableView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self startShowingAds];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self stopShowingAds];
}


#pragma mark - Feed Ads

- (void)startShowingAds {
    if (self.admobAdLoader) {
        return;
    }
    
    NSArray *adTypes = @[ GADAdLoaderAdTypeNative ];
    GADAdLoader *admobAdLoader = [[GADAdLoader alloc] initWithAdUnitID:ADMOB_NATIVEAD_ADUNIT_ID rootViewController:self adTypes:adTypes options:nil];
    admobAdLoader.delegate = self;
    self.admobAdLoader = admobAdLoader;
    
    [admobAdLoader loadRequest:[GADRequest request]];
}

- (void)stopShowingAds {
    self.admobAdIsLoaded = NO;
    
    [self.admobNativeAdView removeFromSuperview];
    self.admobNativeAdView = nil;
    self.admobAdLoader.delegate = nil;
    self.admobAdLoader = nil;
    
    [self updateTableView];
}


#pragma mark GADNativeAdLoaderDelegate

- (void)adLoader:(GADAdLoader *)adLoader didReceiveNativeAd:(GADNativeAd *)nativeAd {
    NSLog(@"***** %s: adLoader = %@, nativeAd = %@", __FUNCTION__, adLoader, nativeAd);
    
    if ([FAGAMMediatedNativeAd isFeedAdNativeAd:nativeAd]) {
        CGRect admobNativeAdViewFrame = CGRectMake(0.0f, 0.0f, 320.0f, 180.0f);
        FAGAMNativeAdView *admobNativeAdView = [[FAGAMNativeAdView alloc] initWithFrame:admobNativeAdViewFrame];
        [admobNativeAdView setupWithNativeAd:nativeAd];
        self.admobNativeAdView = admobNativeAdView;
    } else {
        // TODO Handle other ads
        NSLog(@"***** %s: Received an unexpected ad type", __FUNCTION__);
    }
    
    if (self.admobNativeAdView) {
        self.admobAdIsLoaded = YES;
        [self updateTableView];
    }
}


#pragma mark GADAdLoaderDelegate

- (void)adLoader:(GADAdLoader *)adLoader didFailToReceiveAdWithError:(NSError *)error {
    NSLog(@"***** %s: adLoader = %@, error = %@", __FUNCTION__, adLoader, error);
    
    self.admobAdIsLoaded = NO;
    [self updateTableView];
}


#pragma mark - UITableViewDataSource & UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.displayedData objectAtIndex:indexPath.row];
    
    if ([data isKindOfClass:[FAGAMNativeAdView class]]) {
        FAGAMNativeAdView *adView = (FAGAMNativeAdView *) data;
        
        FAFeedAdTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FAFeedAdTableViewCell class])];
        [cell injectAdView:adView];
        return cell;
    }
    
    if ([data isKindOfClass:[FANewsEntry class]]) {
        FANewsEntryTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FANewsEntryTableViewCell class])];
        [cell setupFromNewsEntry:data];
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.displayedData objectAtIndex:indexPath.row];
    
    if ([data isKindOfClass:[FANewsEntry class]]) {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        FANewsEntryViewController *entryController = [mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([FANewsEntryViewController class])];
        entryController.entry                      = data;
        entryController.title                      = @"";
        
        [self.navigationController pushViewController:entryController animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.displayedData objectAtIndex:indexPath.row];
    
    if ([data isKindOfClass:[FAGAMNativeAdView class]]) {
        FAGAMNativeAdView *adView = (FAGAMNativeAdView *) data;
        return [adView sizeForMaximumWidth:tableView.frame.size.width].height;
    }
    
    return 169.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.displayedData.count;
}

@end

//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FAStandaloneAdViewController.h"
#import <FeedAd/FeedAd.h>


@interface FAStandaloneAdViewController () <FAStandaloneAdDelegate>

@property (nonatomic, weak)   IBOutlet UIView                  *adContainerView;
@property (nonatomic, strong) FAStandaloneAd                   *standaloneAd;
@property (nonatomic, weak)   IBOutlet UIImageView             *testImageView;
@property (nonatomic, weak)   IBOutlet UIButton                *showButton;
@property (weak, nonatomic)   IBOutlet UIActivityIndicatorView *indicatorView;

@end


@implementation FAStandaloneAdViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Resize test image view
    self.testImageView.frame = ({
        CGRect frame      = CGRectZero;
        frame.size.width  = self.view.frame.size.width;
        frame.size.height = roundf(frame.size.width / 16.0f * 9.0f);
        frame.origin.y    = roundf(self.view.frame.size.height / 2.0f - frame.size.height / 2.0f);
        frame;
    });
    
    // Resize ad container view
    self.adContainerView.frame = ({
        CGRect frame      = CGRectZero;
        frame.size.width  = self.view.frame.size.width;
        frame.size.height = roundf(frame.size.width / 16.0f * 9.0f);
        frame.origin.y    = roundf(self.view.frame.size.height / 2.0f - frame.size.height / 2.0f);
        frame;
    });
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self startShowingAds];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self stopShowingAds]; 
}


#pragma mark - Standalone Ad

- (void)startShowingAds {
    if (self.standaloneAd) {
        return;
    }
    
    if ([FAStandaloneAd isAvailableForPlacementId:@"standalone"]) {
        // Setup FAStandaloneAdConfig
        FAStandaloneAdConfig *config = [FAStandaloneAdConfig new];
        config.placementId = @"standalone";
        
        // Setup your FAStandaloneAd instance
        self.standaloneAd = [[FAStandaloneAd alloc] initWithConfig:config];
        self.standaloneAd.delegate = self;
    }
}

- (void)stopShowingAds {
    [self.standaloneAd cancel];
    [self.standaloneAd.adView removeFromSuperview];
    self.standaloneAd.delegate = nil;
    self.standaloneAd = nil;
}


#pragma mark - Actions

- (IBAction)showButtonPressed:(id)sender {
    self.showButton.hidden = YES;
    [self.indicatorView startAnimating];
    
    // Load ad
    [self.standaloneAd load];
}


#pragma mark - FAStandaloneAdDelegate

- (void)standaloneAdDidFinishLoading:(FAStandaloneAd *)standaloneAd {
    NSLog(@"***** %s: standaloneAd = %@", __FUNCTION__, standaloneAd);
    
    [self.indicatorView stopAnimating];
    
    // Setup container view
    self.adContainerView.hidden = NO;
    
    // Add ad view to our container view
    UIView *adView          = self.standaloneAd.adView;
    adView.frame            = self.adContainerView.bounds;
    [self.adContainerView addSubview:adView];
}

- (void)standaloneAdDidChangeSize:(FAStandaloneAd *)standaloneAd {
    NSLog(@"***** %s: standaloneAd = %@", __FUNCTION__, standaloneAd);
    
    CGSize preferredSize = [standaloneAd sizeForSuperviewSize:CGSizeMake(self.view.frame.size.width, CGFLOAT_MAX)];
    
    self.adContainerView.frame = ({
        CGRect frame = self.adContainerView.frame;
        frame.size.width  = preferredSize.width;
        frame.size.height = preferredSize.height;
        frame;
    });
    
    self.adContainerView.center = self.view.center;
}

- (void)standaloneAd:(FAStandaloneAd *)standaloneAd didFailWithError:(NSError *)error {
    NSLog(@"***** %s: standaloneAd = %@, error = %@", __FUNCTION__, standaloneAd, error);
    
    // Hide ad container view and spinner
    self.adContainerView.hidden = YES;
    [self.indicatorView stopAnimating];
}

- (void)standaloneAdDidFinishPlaying:(FAStandaloneAd *)standaloneAd {
    NSLog(@"***** %s: standaloneAd = %@", __FUNCTION__, standaloneAd);
    
    // Hide ad container view
    self.adContainerView.hidden = YES;
}

@end


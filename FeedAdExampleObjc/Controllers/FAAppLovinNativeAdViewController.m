//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FAAppLovinNativeAdViewController.h"
#import <AppLovinSDK/AppLovinSDK.h>
#import "FAFeedAdTableViewCell.h"
#import "FAGlobals.h"
#import "FANewsEntry.h"
#import "FANewsEntryTableViewCell.h"
#import "FANewsEntryViewController.h"
#import "FAAppLovinManualNativeAdView.h"


// See FAGlobals.h for the constant APPLOVIN_KEY

// Replace this with the ID of your AppLovin ad unit
//
// An ad unit must be added in the AppLovin dashboard
// for the bundle ID of your app, before using this example.
// For setup instructions see the AppLovin mediation docs
// at docs.feedad.com.
//
// Please note it might take half an hour or longer for
// any updates in the AppLovin dashboard to take effect
// due to caching.
//
// Also, currently, custom SDK mediation is known to
// only work on real devices -- you may only receive
// test ads from AppLovin on iOS simulator.
#define APPLOVIN_NATIVEAD_ADUNIT_ID @""


@interface FAAppLovinNativeAdViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) MAAd                 *appLovinNativeAd;
@property (nonatomic, strong) MANativeAdView       *appLovinNativeAdView;
@property (nonatomic, strong) MANativeAdLoader     *appLovinNativeAdLoader;
@property (nonatomic, strong) NSMutableArray       *data;
@property (nonatomic, strong) NSMutableArray       *displayedData;
@property (nonatomic, weak)   IBOutlet UITableView *tableView;

@end


@implementation FAAppLovinNativeAdViewController

#pragma mark - Dealloc

- (void)dealloc {
    [self stopShowingAds];
}


#pragma mark - Data

- (NSMutableArray *)injectAds:(NSMutableArray *)data {
    if (!self.appLovinNativeAdView) {
        return data;
    }
    
    NSInteger start        = 2;
    NSInteger gapItemCount = 7;
    
    if (!data || data.count < start) {
        return data;
    }
    
    data = [data mutableCopy];
    
    for (NSInteger i = start; i < data.count; i += gapItemCount) {
        [data insertObject:self.appLovinNativeAdView atIndex:i];
    }
    
    return data;
}

- (void)loadData {
    FANewsEntry *entry = [FANewsEntry dummyEntry];
    
    NSMutableArray *data = [NSMutableArray array];
    
    for (NSInteger i = 0; i < 15; i++) {
        [data addObject:entry];
    }
    
    self.data = data;
}

- (void)updateTableView {
    self.displayedData = [self injectAds:self.data];
    
    [self.tableView reloadData];
}


#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadData];
    [self updateTableView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self startShowingAds];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self stopShowingAds];
}


#pragma mark - Ads

- (void)startShowingAds {
    __weak typeof(self) weakSelf = self;
    
    if (self.appLovinNativeAdLoader) {
        return;
    }
    
    // Initialize the AppLovin SDK
    //
    // Normally this would be done in the app delegate
    ALSdkInitializationConfiguration *appLovinConfig = [ALSdkInitializationConfiguration configurationWithSdkKey:APPLOVIN_KEY builderBlock:^(ALSdkInitializationConfigurationBuilder *builder) {
        builder.mediationProvider = ALMediationProviderMAX;
    }];
    
    [[ALSdk shared] initializeWithConfiguration:appLovinConfig completionHandler:^(ALSdkConfiguration * _Nonnull configuration) {
        typeof(self) strongSelf = weakSelf;
        
        // Initialize MANativeAdView
        //
        // We use a subclass here that implements alternation of the layout for FeedAd
        // The subclass and XIB is part of the example code and can be customized as needed
        UINib *appLovinNativeAdViewNib = [UINib nibWithNibName: @"FAAppLovinManualNativeAdView" bundle: NSBundle.mainBundle];
        strongSelf.appLovinNativeAdView = [appLovinNativeAdViewNib instantiateWithOwner: nil options: nil].firstObject;
        
        // Bind views using the tags specified previously in FAAppLovinManualNativeAdView.xib
        [strongSelf.appLovinNativeAdView bindViewsWithAdViewBinder:[[MANativeAdViewBinder alloc] initWithBuilderBlock:^(MANativeAdViewBinderBuilder * _Nonnull builder) {
            builder.iconImageViewTag = 42;
            builder.titleLabelTag = 43;
            builder.advertiserLabelTag = 44;
            builder.callToActionButtonTag = 45;
            builder.mediaContentViewTag = 46;
        }]];
        
        // Load an ad
        //
        // When using the manual native ad approach, we have to use
        // loadAdIntoAdView: and have an instance of MANativeAdView
        // initialized and bound -- otherwise the SDK will only complain
        // with an error message or views will not be filled with content
        // from the native ad that was received
        MANativeAdLoader *appLovinNativeAdLoader = [[MANativeAdLoader alloc] initWithAdUnitIdentifier:APPLOVIN_NATIVEAD_ADUNIT_ID sdk:[ALSdk shared]];
        appLovinNativeAdLoader.nativeAdDelegate = strongSelf;
        [appLovinNativeAdLoader loadAdIntoAdView:strongSelf.appLovinNativeAdView];
        strongSelf.appLovinNativeAdLoader = appLovinNativeAdLoader;
    }];
}

- (void)stopShowingAds {
    if (self.appLovinNativeAd) {
        [self.appLovinNativeAdLoader destroyAd:self.appLovinNativeAd];
        self.appLovinNativeAd = nil;
    }
    
    if (self.appLovinNativeAdView) {
        [self.appLovinNativeAdView removeFromSuperview];
        self.appLovinNativeAdView = nil;
    }
    
    self.appLovinNativeAdLoader.nativeAdDelegate = nil;
    self.appLovinNativeAdLoader = nil;
    
    [self updateTableView];
}


#pragma mark - UITableViewDataSource & UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.displayedData objectAtIndex:indexPath.row];
    
    if ([data isKindOfClass:[MANativeAdView class]]) {
        MANativeAdView *adView = (MANativeAdView *) data;
        
        FAFeedAdTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FAFeedAdTableViewCell class])];
        [cell injectAdView:adView];
        return cell;
    }
    
    if ([data isKindOfClass:[FANewsEntry class]]) {
        FANewsEntryTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FANewsEntryTableViewCell class])];
        [cell setupFromNewsEntry:data];
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.displayedData objectAtIndex:indexPath.row];
    
    if ([data isKindOfClass:[FANewsEntry class]]) {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        FANewsEntryViewController *entryController = [mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([FANewsEntryViewController class])];
        entryController.entry                      = data;
        entryController.title                      = @"";
        
        [self.navigationController pushViewController:entryController animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.displayedData objectAtIndex:indexPath.row];
    
    if ([data isKindOfClass:[MANativeAdView class]]) {
        return roundf(tableView.frame.size.width / 16.0f * 9.0f) + 1.0f;
    }
    
    return 169.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.displayedData.count;
}


#pragma mark - MANativeAdDelegate

- (void)didClickNativeAd:(MAAd *)ad {
    NSLog(@"***** %s: ad = %@", __FUNCTION__, ad);
}

- (void)didFailToLoadNativeAdForAdUnitIdentifier:(NSString *)adUnitIdentifier withError:(MAError *)error {
    NSLog(@"***** %s: adUnitIdentifier = %@, error = %@", __FUNCTION__, adUnitIdentifier, error);
    
    // You may want to implement retries here
    [self stopShowingAds];
}

- (void)didLoadNativeAd:(MANativeAdView *)nativeAdView forAd:(MAAd *)ad {
    NSLog(@"***** %s: nativeAdView = %@, ad = %@, ad.networkName = %@", __FUNCTION__, nativeAdView, ad, ad.networkName);
    
    // Clean up any pre-existing native ad to prevent memory leaks
    if (self.appLovinNativeAd) {
        [self.appLovinNativeAdLoader destroyAd:self.appLovinNativeAd];
    }

    // Save ad for cleanup
    self.appLovinNativeAd = ad;
    
    // Add ad view to view
    if (self.appLovinNativeAdView) {
        [self.appLovinNativeAdView removeFromSuperview];
    }
    
    self.appLovinNativeAdView = nativeAdView;
    
    // Update table view
    [self updateTableView];
}

@end

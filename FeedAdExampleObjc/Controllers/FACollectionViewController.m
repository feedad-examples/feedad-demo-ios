//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FACollectionViewController.h"
#import <FeedAd/FeedAd.h>
#import "FAFeedAdCollectionViewCell.h"
#import "FAGlobals.h"
#import "FANewsEntry.h"
#import "FANewsEntryCollectionViewCell.h"
#import "FANewsEntryViewController.h"


@interface FACollectionViewController () <FAFeedAdDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, weak)   IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray            *displayedData;
@property (nonatomic, strong) NSMutableArray            *data;
@property (nonatomic, strong) FAFeedAd                  *feedAd;

@end


@implementation FACollectionViewController

#pragma mark - Data

- (NSMutableArray *)injectAd:(NSMutableArray *)data {
    if (!self.feedAd.isLoaded) {
        return data;
    }
    
    NSInteger start        = 2;
    NSInteger gapItemCount = 7;
    
    if (!data || data.count < start) {
        return data;
    }
    
    data = [data mutableCopy];
    
    for (NSInteger i = start; i < data.count; i += gapItemCount) {
        [data insertObject:self.feedAd atIndex:i];
    }
    
    return data;
}

- (void)loadData {
    FANewsEntry *entry = [FANewsEntry dummyEntry];
    
    NSMutableArray *data = [NSMutableArray array];
    
    for (NSInteger i = 0; i < 15; i++) {
        [data addObject:entry];
    }
    
    self.data = data;
}

- (void)updateCollectionView {
    self.displayedData = [self injectAd:self.data];
    
    [self.collectionView reloadData];
}


#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadData];
    [self updateCollectionView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self startShowingAds];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self stopShowingAds];
}


#pragma mark - Feed Ads

- (void)startShowingAds {
    if (self.feedAd) {
        return;
    }
    
    // Setup video ad with placement id
    // The placement id may be chosen by the developer and
    // allows to distinguish different screens
    FAFeedAdConfig *config = [FAFeedAdConfig new];
    config.placementId = @"collection-view";
    
    self.feedAd = [[FAFeedAd alloc] initWithConfig:config];
    self.feedAd.delegate = self;
    
    // Load ad
    [self.feedAd load];
}

- (void)stopShowingAds {
    [self.feedAd cancel];
    [self.feedAd.adView removeFromSuperview];
    self.feedAd.delegate = nil;
    self.feedAd = nil;
    
    [self updateCollectionView];
}


#pragma mark FAFeedAdDelegate

- (void)feedAd:(FAFeedAd *)feedAd didFailWithError:(NSError *)error {
    NSLog(@"***** %s: feedAd = %@, error = %@", __FUNCTION__, feedAd, error);
    
    [self updateCollectionView];
}

- (void)feedAdDidFinishLoading:(FAFeedAd *)feedAd {
    NSLog(@"***** %s: feedAd = %@", __FUNCTION__, feedAd);
    
    [self updateCollectionView];
}

- (void)feedAdDidFinishPlaying:(FAFeedAd *)feedAd {
    NSLog(@"***** %s: feedAd = %@", __FUNCTION__, feedAd);
    
    [self updateCollectionView];
}

- (void)feedAdDidChangeSize:(FAFeedAd *)feedAd {
    NSLog(@"***** %s: feedAd = %@", __FUNCTION__, feedAd);
    
    [self.collectionView.collectionViewLayout invalidateLayout];
}


#pragma mark - UITableViewDataSource & UITableViewDelegate

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.displayedData objectAtIndex:indexPath.row];
    
    if ([data isKindOfClass:[FAFeedAd class]]) {
        FAFeedAd *feedAd = (FAFeedAd *) data;
        
        FAFeedAdCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([FAFeedAdCollectionViewCell class]) forIndexPath:indexPath];
        [cell injectAdView:feedAd.adView];
        return cell;
    }
    
    if ([data isKindOfClass:[FANewsEntry class]]) {
        FANewsEntryCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([FANewsEntryCollectionViewCell class]) forIndexPath:indexPath];
        [cell setupFromNewsEntry:data];
        return cell;
    }
    
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.displayedData objectAtIndex:indexPath.row];
    
    if ([data isKindOfClass:[FANewsEntry class]]) {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        FANewsEntryViewController *entryController = [mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([FANewsEntryViewController class])];
        entryController.entry                      = data;
        entryController.title                      = @"";
        
        [self.navigationController pushViewController:entryController animated:YES];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.displayedData objectAtIndex:indexPath.row];
    
    if ([data isKindOfClass:[FAFeedAd class]]) {
        return [self.feedAd sizeForSuperviewSize:CGSizeMake(self.collectionView.frame.size.width, CGFLOAT_MAX)]; 
    }
    
    return CGSizeMake(collectionView.frame.size.width, 169.0f);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.displayedData.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

@end

//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FAAppLovinMediumRectangleAdViewController.h"
#import <AppLovinSDK/AppLovinSDK.h>
#import "FAFeedAdTableViewCell.h"
#import "FAGlobals.h"
#import "FANewsEntry.h"
#import "FANewsEntryTableViewCell.h"
#import "FANewsEntryViewController.h"
#import "FAAppLovinManualNativeAdView.h"


// See FAGlobals.h for the constant APPLOVIN_KEY

// Replace this with the ID of your AppLovin ad unit
//
// An ad unit must be added in the AppLovin dashboard
// for the bundle ID of your app, before using this example.
// For setup instructions see the AppLovin mediation docs
// at docs.feedad.com.
//
// Please note it might take half an hour or longer for
// any updates in the AppLovin dashboard to take effect
// due to caching.
//
// Also, currently, custom SDK mediation is known to
// only work on real devices -- you may only receive
// test ads from AppLovin on iOS simulator.
#define APPLOVIN_MEDIUMRECTANGLEAD_ADUNIT_ID @""


@interface FAAppLovinMediumRectangleAdViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) MAAdView             *appLovinMediumRectangleAd;
@property (nonatomic, strong) NSMutableArray       *data;
@property (nonatomic, strong) NSMutableArray       *displayedData;
@property (nonatomic, weak)   IBOutlet UITableView *tableView;

@end


@implementation FAAppLovinMediumRectangleAdViewController

#pragma mark - Dealloc

- (void)dealloc {
    [self stopShowingAds];
}


#pragma mark - Data

- (NSMutableArray *)injectAds:(NSMutableArray *)data {
    if (!self.appLovinMediumRectangleAd) {
        return data;
    }
    
    NSInteger start        = 2;
    NSInteger gapItemCount = 7;
    
    if (!data || data.count < start) {
        return data;
    }
    
    data = [data mutableCopy];
    
    for (NSInteger i = start; i < data.count; i += gapItemCount) {
        [data insertObject:self.appLovinMediumRectangleAd atIndex:i];
    }
    
    return data;
}

- (void)loadData {
    FANewsEntry *entry = [FANewsEntry dummyEntry];
    
    NSMutableArray *data = [NSMutableArray array];
    
    for (NSInteger i = 0; i < 15; i++) {
        [data addObject:entry];
    }
    
    self.data = data;
}

- (void)updateTableView {
    self.displayedData = [self injectAds:self.data];
    
    [self.tableView reloadData];
}


#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadData];
    [self updateTableView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self startShowingAds];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self stopShowingAds];
}


#pragma mark - Ads

- (void)startShowingAds {
    __weak typeof(self) weakSelf = self;
    
    if (self.appLovinMediumRectangleAd) {
        return;
    }
    
    // Initialize the AppLovin SDK
    //
    // Normally this would be done in the app delegate
    ALSdkInitializationConfiguration *appLovinConfig = [ALSdkInitializationConfiguration configurationWithSdkKey:APPLOVIN_KEY builderBlock:^(ALSdkInitializationConfigurationBuilder *builder) {
        builder.mediationProvider = ALMediationProviderMAX;
    }];
    
    [[ALSdk shared] initializeWithConfiguration:appLovinConfig completionHandler:^(ALSdkConfiguration * _Nonnull configuration) {
        typeof(self) strongSelf = weakSelf;
        
        // Setup MAAdView and load a medium rectangle ad
        MAAdView *appLovinMediumRectangleAd = [[MAAdView alloc] initWithAdUnitIdentifier:APPLOVIN_MEDIUMRECTANGLEAD_ADUNIT_ID adFormat:MAAdFormat.mrec sdk:[ALSdk shared]];
        appLovinMediumRectangleAd.delegate = strongSelf;
        appLovinMediumRectangleAd.frame = CGRectMake(0.0f, 0.0f, 300.0f, 250.0f);
        strongSelf.appLovinMediumRectangleAd = appLovinMediumRectangleAd;
        
        [appLovinMediumRectangleAd loadAd];
    }];
}

- (void)stopShowingAds {
    if (self.appLovinMediumRectangleAd) {
        [self.appLovinMediumRectangleAd removeFromSuperview];
        self.appLovinMediumRectangleAd.delegate = nil;
        self.appLovinMediumRectangleAd = nil;
    }
    
    [self updateTableView];
}


#pragma mark - UITableViewDataSource & UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.displayedData objectAtIndex:indexPath.row];
    
    if ([data isKindOfClass:[MAAdView class]]) {
        MAAdView *adView = (MAAdView *) data;
        
        FAFeedAdTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FAFeedAdTableViewCell class])];
        [cell injectAdView:adView];
        return cell;
    }
    
    if ([data isKindOfClass:[FANewsEntry class]]) {
        FANewsEntryTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FANewsEntryTableViewCell class])];
        [cell setupFromNewsEntry:data];
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.displayedData objectAtIndex:indexPath.row];
    
    if ([data isKindOfClass:[FANewsEntry class]]) {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        FANewsEntryViewController *entryController = [mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([FANewsEntryViewController class])];
        entryController.entry                      = data;
        entryController.title                      = @"";
        
        [self.navigationController pushViewController:entryController animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.displayedData objectAtIndex:indexPath.row];
    
    if ([data isKindOfClass:[MAAdView class]]) {
        return 250.0f;
    }
    
    return 169.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.displayedData.count;
}


#pragma mark - MAAdViewAdDelegate

- (void)didClickAd:(MAAd *)ad {
    NSLog(@"***** %s: ad = %@", __FUNCTION__, ad);
}

- (void)didCollapseAd:(nonnull MAAd *)ad {
    NSLog(@"***** %s: ad = %@", __FUNCTION__, ad);
}

- (void)didDisplayAd:(nonnull MAAd *)ad {
    NSLog(@"***** %s: ad = %@", __FUNCTION__, ad);
}

- (void)didExpandAd:(nonnull MAAd *)ad {
    NSLog(@"***** %s: ad = %@", __FUNCTION__, ad);
}

- (void)didFailToDisplayAd:(nonnull MAAd *)ad withError:(nonnull MAError *)error {
    NSLog(@"***** %s: ad = %@, error = %@", __FUNCTION__, ad, error);
}

- (void)didFailToLoadAdForAdUnitIdentifier:(NSString *)adUnitIdentifier withError:(MAError *)error {
    NSLog(@"***** %s: adUnitIdentifier = %@, error = %@", __FUNCTION__, adUnitIdentifier, error);
    
    // You may want to implement retries here
    [self stopShowingAds];
}

- (void)didHideAd:(nonnull MAAd *)ad {
    NSLog(@"***** %s: ad = %@", __FUNCTION__, ad);
}

- (void)didLoadAd:(MAAd *)ad {
    NSLog(@"***** %s: ad = %@, ad.networkName = %@", __FUNCTION__, ad, ad.networkName);

    // Update table view
    [self updateTableView];
}

@end

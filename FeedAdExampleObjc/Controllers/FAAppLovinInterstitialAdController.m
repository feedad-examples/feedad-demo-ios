//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FAAppLovinInterstitialAdController.h"
#import <AppLovinSDK/AppLovinSDK.h>
#import "FAGLobals.h"


// See FAGlobals.h for the constant APPLOVIN_KEY

// Replace this with the ID of your AppLovin ad unit
//
// An ad unit must be added in the AppLovin dashboard
// for the bundle ID of your app, before using this example.
// For setup instructions see the AppLovin mediation docs
// at docs.feedad.com.
//
// Please note it might take half an hour or longer for
// any updates in the AppLovin dashboard to take effect
// due to caching.
//
// Also, currently, custom SDK mediation is known to
// only work on real devices -- you may only receive
// test ads from AppLovin on iOS simulator.
#define APPLOVIN_INTERSTITIALAD_ADUNIT_ID @""


typedef NS_ENUM(NSInteger, FAAppLovinInterstitialAdControllerState) {
    FAAppLovinInterstitialAdControllerStateInitializing,
    FAAppLovinInterstitialAdControllerStateInitialized,
    FAAppLovinInterstitialAdControllerStateLoading,
    FAAppLovinInterstitialAdControllerStateLoaded,
    FAAppLovinInterstitialAdControllerStateShowing
};


@interface FAAppLovinInterstitialAdController () <MAAdDelegate>

@property (nonatomic, weak)   IBOutlet UIActivityIndicatorView        *activityIndicatorView;
@property (nonatomic, strong) MAInterstitialAd                        *appLovinInterstitialAd;
@property (nonatomic, weak)   IBOutlet UIButton                       *loadButton;
@property (nonatomic, weak)   IBOutlet UIButton                       *showButton;
@property (nonatomic, assign) FAAppLovinInterstitialAdControllerState  state;

@end


@implementation FAAppLovinInterstitialAdController

#pragma mark - View Lifecycle

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self startShowingAds];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self stopShowingAds];
}


#pragma mark - Interstitial Ad

- (void)startShowingAds {
    __weak typeof(self) weakSelf = self;
    
    if (self.appLovinInterstitialAd) {
        return;
    }
    
    self.state = FAAppLovinInterstitialAdControllerStateInitializing;
    
    // Initialize the AppLovin SDK
    //
    // Normally this would be done in the app delegate
    ALSdkInitializationConfiguration *appLovinConfig = [ALSdkInitializationConfiguration configurationWithSdkKey:APPLOVIN_KEY builderBlock:^(ALSdkInitializationConfigurationBuilder *builder) {
        builder.mediationProvider = ALMediationProviderMAX;
    }];
    
    [[ALSdk shared] initializeWithConfiguration:appLovinConfig completionHandler:^(ALSdkConfiguration * _Nonnull configuration) {
        typeof(self) strongSelf = weakSelf;
    
        // Setup MAInterstitialAd
        MAInterstitialAd *appLovinInterstitialAd = [[MAInterstitialAd alloc] initWithAdUnitIdentifier:APPLOVIN_INTERSTITIALAD_ADUNIT_ID sdk:[ALSdk shared]];
        appLovinInterstitialAd.delegate = strongSelf;
        strongSelf.appLovinInterstitialAd = appLovinInterstitialAd;
        
        strongSelf.state = FAAppLovinInterstitialAdControllerStateInitialized;
    }];
}

- (void)stopShowingAds {
    if (self.appLovinInterstitialAd) {
        self.appLovinInterstitialAd.delegate = nil;
        self.appLovinInterstitialAd = nil;
    }
}


#pragma mark - Actions

- (IBAction)didTapLoadButton:(id)sender {
    self.state = FAAppLovinInterstitialAdControllerStateLoading;
    
    // Load interstitial ad
    [self.appLovinInterstitialAd loadAd];
}

- (IBAction)didTapShowButton:(id)sender {
    self.state = FAAppLovinInterstitialAdControllerStateShowing;
    
    // Show loaded interstitial ad
    [self.appLovinInterstitialAd showAd];
}


#pragma mark States

- (void)setState:(FAAppLovinInterstitialAdControllerState)state {
    BOOL shouldEnableLoadButton = NO;
    BOOL shouldEnableShowButton = NO;
    
    if (state == FAAppLovinInterstitialAdControllerStateInitialized) {
        shouldEnableLoadButton = YES;
    } else if (state == FAAppLovinInterstitialAdControllerStateLoaded) {
        shouldEnableShowButton = YES;
    }
    
    // activityIndicatorView
    self.activityIndicatorView.hidden = state != FAAppLovinInterstitialAdControllerStateInitializing && state != FAAppLovinInterstitialAdControllerStateLoading && state != FAAppLovinInterstitialAdControllerStateShowing;
    
    // loadButton
    self.loadButton.userInteractionEnabled = shouldEnableLoadButton;
    self.loadButton.alpha                  = shouldEnableLoadButton ? 1.0f : 0.5f;
    
    // showButton
    self.showButton.userInteractionEnabled = shouldEnableShowButton;
    self.showButton.alpha                  = shouldEnableShowButton ? 1.0f : 0.5f;
    
    _state = state;
}


#pragma mark - MAAdDelegate

- (void)didClickAd:(MAAd *)ad {
    NSLog(@"***** %s: ad = %@", __FUNCTION__, ad);
}

- (void)didDisplayAd:(MAAd *)ad {
    NSLog(@"***** %s: ad = %@", __FUNCTION__, ad);
}

- (void)didFailToDisplayAd:(MAAd *)ad withError:(MAError *)error {
    NSLog(@"***** %s: ad = %@, error = %@", __FUNCTION__, ad, error);
    
    self.state = FAAppLovinInterstitialAdControllerStateInitialized;
}

- (void)didFailToLoadAdForAdUnitIdentifier:(NSString *)adUnitIdentifier withError:(MAError *)error {
    NSLog(@"***** %s: adUnitIdentifier = %@, error = %@", __FUNCTION__, adUnitIdentifier, error);
    
    self.state = FAAppLovinInterstitialAdControllerStateInitialized;
}

- (void)didHideAd:(MAAd *)ad {
    NSLog(@"***** %s: ad = %@", __FUNCTION__, ad);
    
    self.state = FAAppLovinInterstitialAdControllerStateInitialized;
}

- (void)didLoadAd:(MAAd *)ad {
    NSLog(@"***** %s: ad = %@", __FUNCTION__, ad);
    
    self.state = FAAppLovinInterstitialAdControllerStateLoaded;
}

@end

//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FAAdMobBannerAdViewController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "FAFeedAdTableViewCell.h"
#import "FAGlobals.h"
#import "FANewsEntry.h"
#import "FANewsEntryTableViewCell.h"
#import "FANewsEntryViewController.h"


// Make sure to update GADApplicationIdentifier in your Info.plist
#define ADMOB_BANNERAD_ADUNIT_ID @"<your-admob-adunit-id>"


@interface FAAdMobBannerAdViewController () <GADBannerViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) BOOL                  admobAdIsLoaded;
@property (nonatomic, strong) UIView               *admobBannerAdView;
@property (nonatomic, strong) NSMutableArray       *data;
@property (nonatomic, strong) NSMutableArray       *displayedData;
@property (nonatomic, weak)   IBOutlet UITableView *tableView;

@end


@implementation FAAdMobBannerAdViewController

#pragma mark - Data

- (NSMutableArray *)injectAd:(NSMutableArray *)data {
    if (!self.admobAdIsLoaded) {
        return data;
    }
    
    NSInteger start        = 2;
    NSInteger gapItemCount = 7;
    
    if (!data || data.count < start) {
        return data;
    }
    
    data = [data mutableCopy];
    
    for (NSInteger i = start; i < data.count; i += gapItemCount) {
        [data insertObject:self.admobBannerAdView atIndex:i];
    }
    
    return data;
}

- (void)loadData {
    FANewsEntry *entry = [FANewsEntry dummyEntry];
    
    NSMutableArray *data = [NSMutableArray array];
    
    for (NSInteger i = 0; i < 15; i++) {
        [data addObject:entry];
    }
    
    self.data = [self injectAd:data];
}

- (void)updateTableView {
    self.displayedData = [self injectAd:self.data];
    
    [self.tableView reloadData];
}


#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadData];
    [self updateTableView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self startShowingAds];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self stopShowingAds];
}


#pragma mark - Feed Ads

- (void)startShowingAds {
    if (self.admobBannerAdView) {
        return;
    }
    
    GADBannerView *admobBannerView = [[GADBannerView alloc] initWithAdSize:GADAdSizeFullWidthPortraitWithHeight(250.0f)];
    admobBannerView.adUnitID = ADMOB_BANNERAD_ADUNIT_ID;
    admobBannerView.delegate = self;
    admobBannerView.rootViewController = self;
    self.admobBannerAdView = admobBannerView;
    
    [admobBannerView loadRequest:[GADRequest request]];
}

- (void)stopShowingAds {
    self.admobAdIsLoaded = NO;
    
    [self.admobBannerAdView removeFromSuperview];
    self.admobBannerAdView = nil;
    
    [self updateTableView];
}


#pragma mark - GADBannerViewDelegate

- (void)bannerViewDidReceiveAd:(GADBannerView *)bannerView {
    NSLog(@"***** %s: bannerView = %@", __FUNCTION__, bannerView);
    
    self.admobAdIsLoaded = YES;
    [self updateTableView];
}

- (void)bannerView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(NSError *)error {
    NSLog(@"***** %s: bannerView = %@, error = %@", __FUNCTION__, bannerView, error);
    
    self.admobAdIsLoaded = NO;
    [self updateTableView];
}


#pragma mark - UITableViewDataSource & UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.displayedData objectAtIndex:indexPath.row];
    
    if ([data isKindOfClass:[GADBannerView class]]) {
        FAFeedAdTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FAFeedAdTableViewCell class])];
        [cell injectAdView:data];
        return cell;
    }
    
    if ([data isKindOfClass:[FANewsEntry class]]) {
        FANewsEntryTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FANewsEntryTableViewCell class])];
        [cell setupFromNewsEntry:data];
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.displayedData objectAtIndex:indexPath.row];
    
    if ([data isKindOfClass:[FANewsEntry class]]) {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        FANewsEntryViewController *entryController = [mainStoryboard instantiateViewControllerWithIdentifier:NSStringFromClass([FANewsEntryViewController class])];
        entryController.entry                      = data;
        entryController.title                      = @"";
        
        [self.navigationController pushViewController:entryController animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.displayedData objectAtIndex:indexPath.row];
    
    if ([data isKindOfClass:[GADBannerView class]]) {
        return roundf(tableView.frame.size.width / 16.0f * 9.0f) + 1.0f;
    }
    
    return 169.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.displayedData.count;
}

@end

//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FAInterstitialAdViewController.h"
#import <FeedAd/FeedAd.h>


typedef NS_ENUM(NSInteger, FAInterstitialAdViewControllerState) {
    FAInterstitialAdViewControllerStateUnknown,
    FAInterstitialAdViewControllerStateInitializing,
    FAInterstitialAdViewControllerStateInitialized,
    FAInterstitialAdViewControllerStateLoading,
    FAInterstitialAdViewControllerStateLoaded,
    FAInterstitialAdViewControllerStateShowing
};


@interface FAInterstitialAdViewController () <FAInterstitialAdDelegate>

@property (nonatomic, weak)   IBOutlet UIActivityIndicatorView    *activityIndicatorView;
@property (nonatomic, strong) FAInterstitialAd                    *interstitialAd;
@property (nonatomic, weak)   IBOutlet UIButton                   *loadButton;
@property (nonatomic, weak)   IBOutlet UIButton                   *showButton;
@property (nonatomic, assign) FAInterstitialAdViewControllerState  state;

@end


@implementation FAInterstitialAdViewController

#pragma mark - View Lifecycle

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self startShowingAds];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self stopShowingAds];
}


#pragma mark - Interstitial Ad

- (void)startShowingAds {
    if (self.interstitialAd) {
        [self stopShowingAds];
    }
    
    if ([FAInterstitialAd isAvailableForPlacementId:@"interstitial"]) {
        self.state = FAInterstitialAdViewControllerStateInitializing;
        
        // Setup FAInterstitialAd
        FAInterstitialAdConfig *config = [FAInterstitialAdConfig new];
        config.placementId = @"interstitial";
        
        FAInterstitialAd *interstitialAd = [[FAInterstitialAd alloc] initWithConfig:config];
        interstitialAd.delegate = self;
        self.interstitialAd = interstitialAd;
        
        // Load ad
        self.state = FAInterstitialAdViewControllerStateLoading;
        
        [self.interstitialAd load];
    }
}

- (void)stopShowingAds {
    [self.interstitialAd cancel];
    self.interstitialAd.delegate = nil;
    self.interstitialAd = nil;
    
    self.state = FAInterstitialAdViewControllerStateUnknown;
}


#pragma mark - Actions

- (IBAction)didTapLoadButton:(id)sender {
    [self startShowingAds];
}

- (IBAction)didTapShowButton:(id)sender {
    [self.interstitialAd show];
}


#pragma mark States

- (void)setState:(FAInterstitialAdViewControllerState)state {
    BOOL shouldEnableLoadButton = NO;
    BOOL shouldEnableShowButton = NO;
    
    if (state == FAInterstitialAdViewControllerStateUnknown) {
        shouldEnableLoadButton = YES;
    } else if (state == FAInterstitialAdViewControllerStateInitialized) {
        shouldEnableLoadButton = YES;
    } else if (state == FAInterstitialAdViewControllerStateLoaded) {
        shouldEnableShowButton = YES;
    }
    
    // activityIndicatorView
    self.activityIndicatorView.hidden = state != FAInterstitialAdViewControllerStateInitializing && state != FAInterstitialAdViewControllerStateLoading && state != FAInterstitialAdViewControllerStateShowing;
    
    // loadButton
    self.loadButton.userInteractionEnabled = shouldEnableLoadButton;
    self.loadButton.alpha                  = shouldEnableLoadButton ? 1.0f : 0.5f;
    
    // showButton
    self.showButton.userInteractionEnabled = shouldEnableShowButton;
    self.showButton.alpha                  = shouldEnableShowButton ? 1.0f : 0.5f;
    
    _state = state;
}


#pragma mark - Interstitial Ad Delegate

- (void)interstitialAd:(FAInterstitialAd *)interstitialAd didFailWithError:(NSError *)error {
    NSLog(@"***** %s: interstitialAd = %@, error = %@", __FUNCTION__, interstitialAd, error);
    
    // Cannot reuse FAInterstitialAd, so we'll clean it up here
    [self stopShowingAds];
}

- (void)interstitialAdDidFinishLoading:(FAInterstitialAd *)interstitialAd {
    NSLog(@"***** %s: interstitialAd = %@", __FUNCTION__, interstitialAd);
    
    self.state = FAInterstitialAdViewControllerStateLoaded;
}

- (void)interstitialAdDidFinishPlaying:(FAInterstitialAd *)interstitialAd {
    NSLog(@"***** %s: interstitialAd = %@", __FUNCTION__, interstitialAd);
    
    // Cannot reuse FAInterstitialAd, so we'll clean it up here
    [self stopShowingAds];
}

@end

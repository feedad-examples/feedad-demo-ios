//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#import "FANewsEntryViewController.h"
#import <FeedAd/FeedAd.h>
#import "FAFeedAdTableViewCell.h"
#import "FAGlobals.h"
#import "FANewsEntryParagraphTableViewCell.h"
#import "FANewsEntryTitlesTableViewCell.h"


@interface FANewsEntryViewController () <FAFeedAdDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray *displayedData;
@property (nonatomic, strong) NSMutableArray *data;
@property (nonatomic, strong) FAFeedAd       *feedAd;
@property (nonatomic, weak)   IBOutlet UITableView *tableView;

@end


@implementation FANewsEntryViewController

#pragma mark - Data

- (NSMutableArray *)injectAd:(NSMutableArray *)data {
    if (!self.feedAd.isLoaded) {
        return data;
    }
    
    NSInteger start        = 2;
    NSInteger gapItemCount = 5;
    
    if (!data || data.count < start) {
        return data;
    }
    
    data = [data mutableCopy];
    
    for (NSInteger i = start; i < data.count; i += gapItemCount) {
        [data insertObject:self.feedAd atIndex:i];
    }
    
    return data;
}

- (void)loadData {
    NSMutableArray *data = [NSMutableArray array];
    
    [data addObject:self.entry];
    
    NSArray *paragraphs = [self.entry.body componentsSeparatedByString:@"\n\n"];
    [data addObjectsFromArray:paragraphs];
    
    self.data = data;
}

- (void)updateTableView {
    self.displayedData = [self injectAd:self.data];
    
    [self.tableView reloadData];
}


#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadData];
    [self updateTableView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self startShowingAds];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self stopShowingAds];
}


#pragma mark - Feed Ads

- (void)startShowingAds {
    if (self.feedAd) {
        return;
    }
    
    // Setup video ad with placement id
    // The placement id may be chosen by the developer and
    // allows to distinguish different screens
    FAFeedAdConfig *config = [FAFeedAdConfig new];
    config.placementId = @"news-entry-detail";
    
    self.feedAd = [[FAFeedAd alloc] initWithConfig:config];
    self.feedAd.delegate = self;
    
    // Load ad
    [self.feedAd load];
}

- (void)stopShowingAds {
    [self.feedAd cancel];
    [self.feedAd.adView removeFromSuperview];
    self.feedAd.delegate = nil;
    self.feedAd = nil;
    
    [self updateTableView];
}


#pragma mark FAFeedAdDelegate

- (void)feedAd:(FAFeedAd *)feedAd didFailWithError:(NSError *)error {
    NSLog(@"***** %s: feedAd = %@, error = %@", __FUNCTION__, feedAd, error);
    
    [self updateTableView];
}

- (void)feedAdDidFinishLoading:(FAFeedAd *)feedAd {
    NSLog(@"***** %s: feedAd = %@", __FUNCTION__, feedAd);
    
    [self updateTableView];
}

- (void)feedAdDidFinishPlaying:(FAFeedAd *)feedAd {
    NSLog(@"***** %s: feedAd = %@", __FUNCTION__, feedAd);
    
    [self updateTableView];
}

- (void)feedAdDidChangeSize:(FAFeedAd *)feedAd {
    NSLog(@"***** %s: feedAd = %@", __FUNCTION__, feedAd);
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
}


#pragma mark - UITableViewDataSource & UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.displayedData objectAtIndex:indexPath.row];
    
    if ([data isKindOfClass:[FAFeedAd class]]) {
        FAFeedAd *feedAd = (FAFeedAd *) data;
        
        FAFeedAdTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FAFeedAdTableViewCell class])];
        [cell injectAdView:feedAd.adView];
        return cell;
    }
    
    if ([data isKindOfClass:[FANewsEntry class]]) {
        FANewsEntryTitlesTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FANewsEntryTitlesTableViewCell class])];
        [cell setupFromNewsEntry:data];
        return cell;
    }
    
    if ([data isKindOfClass:[NSString class]]) {
        FANewsEntryParagraphTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([FANewsEntryParagraphTableViewCell class])];
        [cell setupFromParagraph:data];
        return cell;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    id data = [self.displayedData objectAtIndex:indexPath.row];
    
    if ([data isKindOfClass:[FAFeedAd class]]) {
        return [self.feedAd sizeForSuperviewSize:CGSizeMake(self.tableView.frame.size.width, CGFLOAT_MAX)].height;
    }
    
    if ([data isKindOfClass:[NSString class]]) {
        NSString *paragraph = (NSString *) data;
        
        return [FANewsEntryParagraphTableViewCell heightForParagraph:paragraph width:tableView.frame.size.width];
    }
    
    return 90.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.displayedData.count;
}

@end

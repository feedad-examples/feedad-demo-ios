//
// Copyright 2017 - present FeedAd GmbH. All rights reserved.
//
// By using the source code in this repository, you agree to the FeedAd Terms of Service:
// https://feedad.com/tos
//

#define FA_CLIENT_TOKEN @"<your-client-token>"

// Replace this with your AppLovin SDK key
//
// In your own app, you may want to add the SDK key to
// your Info.plist as described in the Getting Started docs
// for the AppLovin SDK.
#define APPLOVIN_KEY @""
